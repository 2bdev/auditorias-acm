// get all the tools we need

//====DEPENDENCIES===========
var createError = require('http-errors');
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var morgan = require('morgan');
var flash = require('connect-flash');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var expressLayouts = require('express-ejs-layouts');

//======================================
var configDB = require('./config/db');
const questionRouter = require('./routes/question');
const userRouter = require('./routes/user');
const auditRouter = require('./routes/audit');
const trelloRouter = require('./routes/trelloCards');
const asinRouter = require('./routes/asin');
const listRouter = require('./routes/listados');
const trellouserRouter = require('./routes/trellousers');
const departamentosRouter = require('./routes/departamentos');
const countriesRouter = require('./routes/country');
const brandsRouter = require('./routes/brand');
const requestRouter = require('./routes/request_type');
//===========================================




//==========CONFIGURACION=================
mongoose.connect(configDB.url, {
  useNewUrlParser: true,
  useFindAndModify: false
}); // connect to our database
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

/*
const mongoDB = process.env.MONGODB_URI || configDB.dev_db_url;
mongoose.connect(mongoDB, {
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
*/

require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev'));
//app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
//app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



//======== view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layouts/main');
app.use(expressLayouts);
//==================================


//====== required for passport
app.use(session({
  secret: 'dreamsandwishwithallmyheart',
  resave: true,
  saveUninitialized: true
}));


app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

//====================

// routes ======================================================================
require('./routes/index.js')(app, passport); // load our routes and pass in our app and fully configured passport
app.use('/questions', questionRouter);
app.use('/users', userRouter);
app.use('/audits', auditRouter);
app.use('/trello', trelloRouter);
app.use('/asins', asinRouter);
app.use('/listados', listRouter);
app.use('/trellousers', trellouserRouter);
app.use('/departamentos', departamentosRouter);
app.use('/countries', countriesRouter);
app.use('/brands', brandsRouter);
app.use('/requests', requestRouter);
//======================================


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
function errorHandler(err, req, res) {
  // set locals, only providing error in development
  console.log('err');
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);

  console.log("Error: " + res.locals.error.status);
  console.log("Message: " + res.locals.message);
  console.log("Description: " + res.locals.error.stack);
  res.render('error', {
    layout: 'layouts/error',
    extractScripts: true,
    extractStyles: true,
    title: 'Error: ' + res.locals.error.status,
    message: 'Msg: ' + res.locals.message
  });
}

// error handler
app.use(function(err, req, res, next) {
  errorHandler(err, req, res);
});

module.exports = app;
