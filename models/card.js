const mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
const Schema = mongoose.Schema;
var Audit = require('../models/audit');
var User = require('../models/user');
var cardsSchema = new Schema({
  //necesario para crear card
  name: String,
  desc: String,
  pos: {
    type: String,
    default: 'pos'
  },
  idList: String, //REQUIRED
  due: {
    type: String,
    default: 'null'
  },
  dueComplete: {
    type: Boolean,
    default: false
  },
  idMembers: String,
  urlSource: String,
  // card Control
  linkBoard: String,
  userAutor: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    autopopulate: true
  },
  audit: {
    type: Schema.Types.ObjectId,
    ref: 'Audit',
    autopopulate: true
  },
  idLabel: String,
  country: String
});

cardsSchema.plugin(timestamps);
cardsSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Cards', cardsSchema);
