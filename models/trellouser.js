// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var Schema = mongoose.Schema;
const Department = require('../models/department');

// define the schema for our user model
var trellousersSchema = mongoose.Schema({
  email : String,
  userName: String,
  id_user: String,
  dptoUser: {
    type: Schema.Types.ObjectId,
    ref: 'Department',
    autopopulate: true
  }
});


trellousersSchema.plugin(timestamps);
trellousersSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('TrelloUser', trellousersSchema);
