// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TrelloUser = require('../models/trellouser');

//var TrelloUser = require('../models/trellousers');

// define the schema for our user model
var userSchema = mongoose.Schema({
  signupDate: {
    type: Date,
    default: Date.now()
  },
  tipo: String,
  email: String,
  password: String,
  trellouser: {
    type: Schema.Types.ObjectId,
    ref: 'TrelloUser',
    autopopulate: true
  },
  marcas: [String],
  status : {
    type: Number,
    default: 1
  }

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};
userSchema.plugin(timestamps);
userSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('User', userSchema);



//======
/*
Obtener todas las marcas disponibles
y cargarlas en un select para agregarla al usuario.

*/



//=======
