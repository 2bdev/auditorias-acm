// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

// define the schema for our user model
var brandSchema = mongoose.Schema({
  name: String,
  shortName: String,
  status: {
    require: true,
    type: Number,
    default: 1
  }
});

brandSchema.plugin(timestamps);
// create the model for users and expose it to our app
module.exports = mongoose.model('Brand', brandSchema);
