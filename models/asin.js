// load the things we need
var mongoose = require('mongoose');
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var asinSchema = mongoose.Schema({

  brand: String,
  canal: String,
  asinParent: String,
  status: {type: Number, default: 1},
  audits: [String],
  isBusinessReport : Boolean,
  last_audit: Date,
  sku : String,
  country: {type: String, default: 'US'}

});
asinSchema.plugin(timestamps);
//asinSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Asin', asinSchema);

/*
Las auditorias se realizan por departamento:
-1 Optimizacion y Contenido
-2 Programacion
-3 Account

En el array de audits se registra el nro
segun el tipo de auditoria que se le ha realizado
y este se actualiza cada vez que es cargado el enlace en la pagina.


*/
