// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var departmentsSchema = mongoose.Schema({
  name: String,
  tipo: {
    type: String,
    default: 'a'
  },
  idBoard: String,
  linkBoard: String,
  nameBoard: String,
  nameList: String,
  idList: String,
  variables: [String],
  status : {
    type: Number,
    default: 1
  }
});
departmentsSchema.plugin(timestamps);

// create the model for users and expose it to our app
module.exports = mongoose.model('Department', departmentsSchema);

/*
Los departments:
  - Nombre
  - Link del Tablero
  - Lista
*/
