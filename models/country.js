// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

var countrysSchema = mongoose.Schema({
  name: String,
  url: String
});

countrysSchema.plugin(timestamps);
module.exports = mongoose.model('Country', countrysSchema);
