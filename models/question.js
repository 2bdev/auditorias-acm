const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Department = require('../models/department');

var questionSchema = new Schema({

  contentQuestion: {
    require: true,
    type: String
  },
  typeQuestion: { //variable
    require: true,
    type: String
  },
  statusQuestion: {
    require: true,
    type: Number,
    default: 1
  },
  dptoQuestion: {
    type: Schema.Types.ObjectId,
    ref: 'Department',
    autopopulate: true
  }
});

/*
Tipos Preguntas
1. Titulo
2. Descripcion y Contenido

Departamentos
-1 Optimizacion y Contenido
-2 Programacion
-3 Account

Status
0. Inactivo
1. Activo

LinkBoard: el board en el que se registra la tarjeta
util  para registrar link donde esta la tarjeta.

*/
questionSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Question', questionSchema);
