const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var timestamps = require('mongoose-timestamp');
const moment = require('moment');
//const moment = require('moment');
const Asin = require('../models/asin');
const User = require('../models/user');
const Card = require('../models/card');
const Department = require('../models/department');
//moment(audits[i].fecha_audit).format("MM/DD/YYYY hh:mm a")
var auditSchema = new Schema({
  tipo: {
    type: Schema.Types.ObjectId,
    ref: 'Department',
    autopopulate: true
  },
  link: {
    type: Schema.Types.ObjectId,
    ref: 'Asin',
    autopopulate: true
  },
  usuario: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    autopopulate: true
  },
  fecha_audit: {
    type: Date,
    default: moment().format()
  },
  audit_ref: String,
  audit_nro: Number,
  evaluacion: [{}], //array de objetos preguntas y respuestas  {variable, dpto,  pregunta, respuesta}
  result: Number,
  country: String

});
/*[{
    variable: String,
    dpto: String,
    respuestas: [{
      rpt: String,
      prg: String
  }],
  result: Number
}]*/

auditSchema.plugin(timestamps);
auditSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Audit', auditSchema);

/*
  Tipos de auditoria, segun dpto
  -1 Optimizacion y Contenido
  -2 Programacion
  -3 Account


  Variables segun tipo de auditoria
  1. Titulo
  2. Descripcion y Contenido
*/
