// load the things we need
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var Schema = mongoose.Schema;
const Department = require('../models/department');

// define the schema for our user model
var requesttypesSchema = mongoose.Schema({
  name: String,
  depto: {
    type: Schema.Types.ObjectId,
    ref: 'Department',
    autopopulate: true
  },
  status: {
    require: true,
    type: Number,
    default: 1
  }
});

requesttypesSchema.plugin(timestamps);
requesttypesSchema.plugin(require('mongoose-autopopulate'));
// create the model for users and expose it to our app
module.exports = mongoose.model('RequestType', requesttypesSchema);
