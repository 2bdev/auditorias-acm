$(document).ready(function(){
  $(document).on ("click", "#sync", function(){
    var data = {};//send data to convert
    data.asins = JSON.stringify(array);
    $.ajax({
      type: "POST",
      url: "/asins/upload_file",
      data: data,
      success: function(data){
        swal("Bien!", "Proceso completado", "success")
        window.location.replace('/asins');
      },
      async:   false
    });
  });
  $("#viewfile").click(function(){
    ExportToTable();
  });
  //RefreshItemList
  var array =  [];
  var RefreshItemList = function(){
    var total = 0;
    var html = "";
    html+= "<table class='table table-responsive'><tbody>";
    html+= "<tr>";
    html+= "<th>ASIN</td>";
    html+= "<th>SKU</td>";
    html+= "<th>BRAND</td>";
    html+= "<th>COUNTRY</td>";
    html+= "</tr>";
    for(var n in array){
      html+= "<tr>";
      html+= "<td>"+array[n].asin+"</td>";
      html+= "<td>"+array[n].sku+"</td>";
      html+= "<td>"+array[n].brand+"</td>";
      html+= "<td>"+array[n].country+"</td>";
      html+= "</tr>";
    }
    html+= "</tbody></table>";
    $(".item_list").html(html);
  };
  //import table
  function ExportToTable() {
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
    /*Checks whether the file is a valid excel file*/
    if (regex.test($("#excelfile").val().toLowerCase())) {
      var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
      if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
        xlsxflag = true;
      }
      /*Checks whether the browser supports HTML5*/
      if (typeof (FileReader) != "undefined") {
        var reader = new FileReader();
        reader.onload = function (e) {
          var data = e.target.result;
          /*Converts the excel data in to object*/
          if (xlsxflag) {
            var workbook = XLSX.read(data, { type: 'binary' });
          }
          else {
            var workbook = XLS.read(data, { type: 'binary' });
          }
          /*Gets all the sheetnames of excel in to a variable*/
          var sheet_name_list = workbook.SheetNames;

          var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/
          sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/
            /*Convert the cell value to Json*/
            if (xlsxflag) {
              var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
            }
            else {
              var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
            }
            if (exceljson.length > 0 && cnt == 0) {
              BindTable(exceljson, '#exceltable');
              cnt++;
            }
          });
          $('#exceltable').show();
        }
        if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
          reader.readAsArrayBuffer($("#excelfile")[0].files[0]);
        }
        else {
          reader.readAsBinaryString($("#excelfile")[0].files[0]);
        }
      }
      else {
        swal("Lo siento!", "Tu navegador no soporta HTML5!", "warning")

      }
    }
    else {
      swal("Por favor", "sube  un archivo de excel valido", "danger")
      
    }
  }
  function BindTable(jsondata, tableid) {/*Function used to convert the JSON array to Html Table*/
    var columns = BindTableHeader(jsondata, tableid); /*Gets all the column headings of Excel*/
    for (var i = 0; i < jsondata.length; i++) {
      var o = new Object();
      for (var colIndex = 0; colIndex < columns.length; colIndex++) {
        switch(columns[colIndex]) {
          case "ASIN":
          o.asin = jsondata[i][columns[colIndex]]
          break;
          case "SKU":
          o.sku = jsondata[i][columns[colIndex]]
          break;
          case "BRAND":
          o.brand = jsondata[i][columns[colIndex]]
          break;
          case "COUNTRY":
          o.country = jsondata[i][columns[colIndex]]
          break;
        }
      }
      array.push(o);
    }
    RefreshItemList();
  }
  function BindTableHeader(jsondata, tableid) {/*Function used to get all column names from JSON and bind the html table header*/
    var columnSet = [];
    for (var i = 0; i < jsondata.length; i++) {
      var rowHash = jsondata[i];
      for (var key in rowHash) {
        if (rowHash.hasOwnProperty(key)) {
          if ($.inArray(key, columnSet) == -1) {/*Adding each unique column names to a variable array*/
            columnSet.push(key);
          }
        }
      }
    }
    return columnSet;
  }
});
