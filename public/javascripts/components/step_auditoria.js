var datos = {};
var auditoria = {};
var card = {//necesario para crear card
  name: String,
  desc: String,
  idList: String, //REQUIRED
  idMembers: String, //string de id trellousers separados por coma
  idLabels: String,
  urlSource: String, // url de producto
  due: String,
  // card Control
  linkBoard: String, // id del board donde se creara la tarjeta
  userAutor: String, // id del usuario logueado
  audit: String // id del audit que se esta realizando
};
var evaluacion = [];
var statuscard = false;
var totalQ = 0;

function generarEvaluacion(arr, v, p, r){
var index = arr.findIndex(function(item, i){
  return item.variable === v;
});
if(index != -1){
  var ind = arr[index].result.findIndex(function(item, i){

    return item.p === p;
  });
  if(ind != -1){
    arr[index].result[ind].r = r;
  }
}
  // items en false son los que están ok
  const totalOK = arr[index].result.filter(item => item.r == false );
console.log(totalOK);
console.log(arr[index].totalV);
  arr[index].porcentaje = (totalOK.length*100)/arr[index].totalV;
  //return arr[index].totalV;

}

$(document).ready(function(){
  $('.input-group.date').datepicker({
    format: "mm-dd-yyyy",
    startDate: "today",
    clearBtn: true,
    language: "es"
});
  $("#select-audit").show();
  $("#make-audit").hide();

  $("#wizard").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true,
    // enablePagination: false,
    enableKeyNavigation: false,
    onFinishing: function (event, currentIndex){
        console.log('datos FINISH');
        console.log(datos);
        goToAudit(datos)
        return true;
    }
  });

  //departamento
  fetch(`/departamentos/get/departamentos`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    //console.log(data);
    $.each(data, function(key, value) {
      //console.log(data[key]);
      console.log(data[key].tipo);
      if(data[key].tipo == 'a'){
        $("#departamento").append('<p> <input data-board="'+data[key].idBoard+'" type="radio" name="depto" class="radio-depto m-r-xs" value="'+data[key]._id+'"> <label>'+data[key].name+'</label> </p>');

      }
    }); // close each()
    $(".radio-depto").change(function() {
      datos.depto_id = $("input[name='depto']:checked"). val();
      datos.idBoard = $("input[name='depto']:checked").attr('data-board');
      datos.depto = $("input[name='depto']:checked").next('label').text();
      //console.log(datos);
      $("#audit_data").append('<p> DEPARTAMENTO: <span class="label label-success">'+ datos.depto+'</span></p>')
      $("#wizard").steps("next",{});
    });
  })
  .catch(err => {
    console.log(`Err: ${err}`);
  });


  //countries
  fetch(`/countries/get/countries`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    $.each(data, function(key, value) {
      $("#pais").append('<p> <input type="radio" name="pais" class="radio-pais m-r-xs" value="'+data[key]._id+'"> <label>'+data[key].name+'</label> </p>');
    }); // close each()
    $(".radio-pais").change(function() {
      datos.pais_id = $("input[name='pais']:checked").val();
      datos.pais = $("input[name='pais']:checked").next('label').text();
      //console.log(datos);
      $("#audit_data").append('<p>&nbsp;PAIS: <span class="label label-info">'+datos.pais+'</span></p>')
      $("#wizard").steps("next",{});
      var iduser = $('td.user').attr('data-user');

      brandsDisponibles(datos.depto_id, datos.pais_id, iduser);
    });
  })
  .catch(err => {
    console.log(`Err: ${err}`);
  });
});

/*GET Brands*/
function brandsDisponibles(dpto, pais, iduser){
  //console.log(dpto);
  //console.log(pais);
  //console.log(iduser);
  $('#marca').empty();
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');

  fetch(`/users/get/user/${iduser}`,
     {method: 'GET'})
     .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    var marcas = data.marcas;
    console.log(marcas);
//========================
fetch(`/brands/get/brands`,
  //`/asins/get/brands`,
   {method: 'GET'})
   .then(response => {
  if (response.ok) {
    return response.json();
  } else {
    return Promise.reject({
      status: response.status,
      statusText: response.statusText
    })
  }
}).then(data => {
  console.log('marcas');
  console.log(data);
  $('#spinner').html("");
  $('#spinner').removeClass('loader');
  markas = data.filter(b => b.status == 1);
  console.log(markas);
if(markas.length > 0){
    for(var i = 0;  i < marcas.length; i++){
        $.each(markas, function(key, value){

            if(marcas[i] === value.shortName){
              $("#marca").append('<p>'+
              '<button type="button" class="list-group-item list-group-item-action">'+
              `<input type="radio" name="marca" class="radio-marca m-r-xs" value="${markas[key].shortName}">`+
               `<label>${markas[key].shortName.trim()}</label></button></p>`);
             }
           }); // close each()
         }// close for()
    $(".radio-marca").change(function() {
      datos.marca = $("input[name='marca']:checked").val();
      //console.log('cambia marca');
      $("#audit_data").append('<p>&nbsp;Marca: <span class="label label-primary">'+datos.marca+'</span></p>')
      $("#wizard").steps("next",{});
      //console.log(datos);
      AsinbyBrand(datos)
    });
  }else{
    var msj = '<p>No hay productos de ninguna marca disponibles para realizarle auditoria.</p>';
    $('#marca').append(msj);
  }
}).catch(err => {
  console.log(`Err: ${err}`);
});

  //=======================
  })


}
/*GET Asins by brand*/
function AsinbyBrand(datos){
  //console.log('ejecutando asin by brand');
  $('#asin').empty();
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');
  //console.log(datos.marca);
  //console.log(datos);
  fetch(`/asins/get/audit/${datos.depto_id}_${datos.pais}/brand/${datos.marca}`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    //console.log(data);
    datos.idAsin = data._id;
    //console.log(datos);
    $('#spinner').html("");
    $('#spinner').removeClass('loader');
    if(data.length > 0 ){
      $.each(data, function(key, value) {
        $("#asin").append(`<p><button type="button" class="list-group-item list-group-item-action">`+
        `<input data-idasin="${data[key]._id}" type="radio" name="marca" class="radio-marca m-r-xs" value="${data[key].asinParent}">`+
         `<label>${data[key].asinParent}  -- SKU: ${data[key].sku} </label></button></p>`);
      }); // close each()
      $(".radio-marca").change(function() {
        //console.log('seleccionando marca');
        //console.log($("input[name='marca']:checked").val());
        datos.asin = $("input[name='marca']:checked").val();
        datos.idAsin = $("input[name='marca']:checked").attr('data-idasin');
        //console.log(datos);
        $("#audit_data").html(
          `<p> DEPARTAMENTO: <span class="label label-success">${datos.depto}</span></p>`+
          `<p>&nbsp;PAIS: <span class="label label-info">${datos.pais}</span></p>`+
          `<p>&nbsp;Marca: <span class="label label-primary">${datos.marca}</span></p>`+
          `<p>&nbsp;ASIN: `+
          `<span class="label label-warning-light float-right">${datos.asin}</span></p>`
          )
        $("#wizard").steps("next",{});
      });
    }else{
      var msj = '<p>No hay asin de esta marca disponibles para realizarle auditoria.</p>';
      $('#asin').append(msj);
    }
  }).catch(err => {
    console.log(`Err: ${err}`);
  });
}
function goToAudit(datos){
  console.log('goToAudit');
  //console.log(datos);
  var dataUpdate = {
    audits: `${datos.depto_id}_${datos.pais}`
  };
  //console.log(dataUpdate);
  swal({
    title: "Estas seguro?",
    text: `Empezarás una auditoria de ${datos.depto} para el asin ${datos.asin}`,
    type: "warning",
    buttons: true,
    dangerMode: true,
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Si, auditar!",
    closeOnConfirm: true
  }, function(){
    console.log('ACEPTAR go to audit');
    //loader
    $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
    $('#spinner').addClass('loader');


    $("#select-audit").fadeOut();
    $(".page-heading").fadeOut();
    $("#make-audit").show();
    $("body").addClass("mini-navbar");
    $("#depto_name").text(datos.depto)
    $("#asinparent").text(datos.asin)
    if (datos.pais == 'US') {
      datos.url = 'https://www.amazon.com/dp/'+datos.asin+'?th=1&psc=1'
      //console.log(datos);
      //console.log(datos.url);
      $('#enlaceasin').empty();
      $('#enlaceasin').append(`<a href='${datos.url}' target="_blank">${datos.url}</a>`);
    }
    $("#asinmarca").text(datos.marca)
    $("#iframeProduct").attr('src', datos.url);
    // Retornando las preguntas del dpto
    fetch(`/questions/get/question_audit/${datos.depto_id}`, { method: 'GET' }) .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    }).then(data => {
      console.log(data);
      totalQ = data.questions.length;
      //console.log(totalQ);


      $.each(data.tipos, function(key, value) {
        var item = {
          variable: String,
          result: [],
         totalV: Number,
        };
          item.variable = data.tipos[key];
          console.log(item.variable);
          var n = 0;
            for(var i = 0; i < data.questions.length; i ++){
              if(data.tipos[key] == data.questions[i].typeQuestion){
                console.log(n);
                item.result.push({p: data.questions[i].contentQuestion });
                n++;
              }
            }
            item.totalV = n;
            evaluacion.push(item);
      });
      //console.log(evaluacion);

      if (data) {
        if($('#wizard_audit .steps.clearfix').length == 0){
          console.log(data.tipos);
          $.each(data.tipos, function(key, value) {
            $("#wizard_audit").append('<p><h3>'+data.tipos[key]+'</h3> <section> <div id="'+data.tipos[key]+'""> <p>Selecciona los items que deseas añadir a una tarjeta en trello</p> <div class="ibox-content audit-content-box"> <div class="panel-body panel-audit-body"> <div class="panel-group"> <div class="panel panel-default" id="'+data.tipos[key]+'-questions"> </div> </div> </div> <div class="btn-group btn-group-toggle" data-toggle="buttons" style="padding-left: 25%;">  <button  type="button" class="btn btn-danger btn_NO '+data.tipos[key]+'" data-variable="'+data.tipos[key]+'" data-toggle="modal" data-target="#cardModal" style="background-color: #gray;" disabled="disabled"> Crear Tarjeta </button> </div> </div> </section></p>');
            }); // close each()
        }
        if($('#wizard_audit .content.clearfix').length == 0){
          console.log(data.questions);
          $.each(data.questions, function(key, value) {
            $("#"+data.questions[key].typeQuestion+"-questions").append('<div data-check="false" class="panel-heading" > <div class="checkbox m-r-xs"> <input type="checkbox" class="pregunta '+data.questions[key].typeQuestion+'" data-variable="'+data.questions[key].typeQuestion+'"  indeterminate name="'+data.questions[key]._id+'" value="'+data.questions[key].contentQuestion+'"> <label>'+data.questions[key].contentQuestion+'</label></div></div>');
          }); // close each()
        }
        $("#wizard_audit").steps({
          headerTag: "h3",
          bodyTag: "section",
          transitionEffect: "slideLeft",
          autoFocus: true,
          enableKeyNavigation: false,
          onFinishing: function (event, currentIndex){
            //console.log('datos_audit FINISH');
            //console.log(datos);
            //TODO SAVE DATA!
            return true;
          }
        });
        $(".pregunta").change(function(){
          //console.log($(".pregunta"));
          var size = $(".pregunta").length;
          var n = 0;
          $(".pregunta").each(function() {
            if ($(this).prop('checked')== true ){
              //items evaluados de forma NOoK, se agregan a tarjeta
              var p = $(this).val()
              var tipo = $(this).attr('data-variable');
              generarEvaluacion(evaluacion, tipo, p, true);

            }else{
              //items evaluados de forma OK, no se agregan a tarjeta
              var p = $(this).val()
              var tipo = $(this).attr('data-variable');
              generarEvaluacion(evaluacion, tipo, p, false);

            }
          });

            for(var i = 0; i < data.tipos.length; i ++){
              n=0;
              $(`.pregunta.${data.tipos[i]}`).each(function() {
                  if ($(this).prop('checked') == true){
                    n++;
                    console.log(n);
                  }
                });
                console.log();
                if(n <= $(`.pregunta.${data.tipos[i]}`).length && n != 0){
                  console.log(n);
                      $(`.btn_NO.${data.tipos[i]}`).removeAttr('disabled').css("background-color", "#ec4758");
                }
                if(n == 0){
                  console.log('n=0');
                  $(`.btn_NO.${data.tipos[i]}`).attr('disabled', 'disabled');
                }
            }
        });
      }
    })
    .catch(err => {
      console.log(`Err: ${err}`);
    });//fin then data
    // /updateAsin/
    //console.log('creare un audit');
    console.log(datos);
      auditoria.tipo = datos.depto_id
      auditoria.link = datos.idAsin
      auditoria.usuario = $('td.user').attr('data-user')
      auditoria.country = datos.pais

    console.log(auditoria);
    fetch(`/audits/create/audit`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(auditoria)
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .catch(err => {
        console.log(`Err: ${err}`);
      });
      fetch(`/asins/update/asin/audits/${datos.idAsin}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(dataUpdate)
        })
        .then(response => {
          if (response.ok) {
            console.log(response.status);
            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .catch(err => {
          console.log(`Err: ${err}`);
        });
        setTimeout(function(){
          $('#spinner').html("");
          $('#spinner').removeClass('loader');
         }, 2000);


  }) //fin function al aceptar sweetalert
}
//=============fin goToAudit =========================

//============ nextAudit =============

function nextAudit(datos){

  //console.log('nextAudit');
  //console.log(datos);
  var dataUpdate = {
    audits: `${datos.depto_id}_${datos.pais}`
  };
  //console.log(dataUpdate);
  swal({
    title: "Estas seguro?",
    text: `Empezarás una auditoria de ${datos.depto} para el asin ${datos.asin}`,
    type: "warning",
    buttons: true,
    dangerMode: true,
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Si, auditar!",
    closeOnConfirm: true
  }, function(){
    //console.log('ACEPTAR go to audit');

    $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
    $('#spinner').addClass('loader');


    $("#make-audit").show();
    $("#depto_name").text(datos.depto)
    $("#asinparent").text(datos.asin)
    if (datos.pais == 'US') {
      datos.url = 'https://www.amazon.com/dp/'+datos.asin+'?th=1&psc=1'
      //console.log(datos);
      //console.log(datos.url);
      $('#enlaceasin').empty();
      $('#enlaceasin').append(`<a href='${datos.url}' target="_blank">${datos.url}</a>`);
    }
    $("#asinmarca").text(datos.marca)
    $("#iframeProduct").attr('src', datos.url);
    // /get/question_audit/
    fetch(`/questions/get/question_audit/${datos.depto_id}`, { method: 'GET' }) .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    }).then(data => {
      //console.log(data);
      totalQ = data.questions.length;
      //console.log(totalQ);


      $.each(data.tipos, function(key, value) {
        var item = {
          variable: String,
          result: [],
         totalV: Number,
        };
          item.variable = data.tipos[key];
          //console.log(item.variable);
          var n = 0;
            for(var i = 0; i < data.questions.length; i ++){
              if(data.tipos[key] == data.questions[i].typeQuestion){
                console.log(n);
                item.result.push({p: data.questions[i].contentQuestion });
                n++;
              }
            }
            item.totalV = n;
            evaluacion.push(item);
      });
      console.log(evaluacion);

      if (data) {
        $(".pregunta").change(function(){
          //console.log($(".pregunta"));
          var size = $(".pregunta").length;
          var n = 0;
          $(".pregunta").each(function() {
            if ($(this).prop('checked')==false){
              //items evaluados de forma NOoK, se agregan a tarjeta
              var p = $(this).val()
              var tipo = $(this).attr('data-variable');
              generarEvaluacion(evaluacion, tipo, p, false);

            }else{
              //items evaluados de forma OK, no se agregan a tarjeta
              var p = $(this).val()
              var tipo = $(this).attr('data-variable');
              generarEvaluacion(evaluacion, tipo, p, true);

            }
          });

            for(var i = 0; i < data.tipos.length; i ++){
              n=0;
              $(`.pregunta.${data.tipos[i]}`).each(function() {
                  $(`.btn_NO[data-variable='${data.tipos[i]}']`);
                  if ($(this).prop('checked') == true){
                    n++;
                  }
                });
                if($(`.pregunta.${data.tipos[i]}`).length <= n && n != 0){
                  //console.log(n);
                      $(`.btn_NO.${data.tipos[i]}`).removeAttr('disabled').css("background-color", "#ec4758");
                }
                if(n == 0){
                  //console.log('n=0');
                  $(`.btn_NO.${data.tipos[i]}`).attr('disabled', 'disabled').css("background-color", "gray");
                }
            }
        });
      }
    })//fin then data
    .catch(err => {
      console.log(`Err: ${err}`);
    });
    // /updateAsin/
    console.log('creare un next audit');
    //console.log(datos);
    console.log($('td.user').attr('data-user'));
    auditoria.tipo = datos.depto_id
    auditoria.link = datos.idAsin
    auditoria.usuario = $('td.user').attr('data-user')
    auditoria.country = datos.pais
    fetch(`/audits/create/audit`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: auditoria
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .catch(err => {
        console.log(`Err: ${err}`);
      });
      fetch(`/asins/update/asin/audits/${datos.idAsin}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(dataUpdate)
        })
        .then(response => {
          if (response.ok) {
            console.log(response.status);
            return response.json();

          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .catch(err => {
          console.log(`Err: ${err}`);
        });
        setTimeout(function(){
          $('#spinner').html("");
          $('#spinner').removeClass('loader');
         }, 2000);

  }) //fin function al aceptar sweetalert
}


//=========== fin nextAudit===============
//card
$('#cardModal').on('show.bs.modal', function(event) {

//  $('#openModal').modal('show');

  //console.log($("select#solicitud_type option"));
  //console.log($("select#solicitud_type option").length);
  var button = $(event.relatedTarget) // Button that triggered the modal
  console.log(button);
  variable = button[0].dataset.variable;
  console.log(variable);
  //Estandar MARCA-PAIS-CANAL-"SOLICITUD
  $('#card-titulo').val(`${datos.marca}-AMZ-${datos.pais}`)
  //Tipo de solicitud
  if($("select#solicitud_type option").length == 1 ){
    $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
    $('#spinner').addClass('loader');

  fetch(`/requests/get/requests/${datos.depto_id}`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    //console.log(data);
    if (data) {
      $.each(data, function(key, value) {
        $("select#solicitud_type").append('<option value="'+data[key].name+'">'+data[key].name+'</option>');
      }); // close each()
    }
  })
  .catch(err => {
    console.log(`Err: ${err}`);
  });
}
  $("select[name=solicitud_type]").change(function() {
    $("#solicitud_type option:selected").attr("selected", "selected");
    //console.log($("#solicitud_type option:selected"));

      var solicitud = $("#solicitud_type option:selected").val();
      $('#card-titulo').val(`${datos.marca}-AMZ-${datos.pais}-${solicitud}-AUDITORIA`)


  });

  if($("select#priority option").length == 1 ){
    ///labels/get/labels
    var boardcito = datos.idBoard;
    fetch(`/departamentos/get/labeltrello/${boardcito}`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    //console.log(data);
    if (data) {
      $.each(data, function(key, value) {
        $("select#priority").append('<option value="'+data[key].id+'">'+data[key].name+'</option>');
      }); // close each()
    }
    $('#spinner').html("");
    $('#spinner').removeClass('loader');
  })
  .catch(err => {
    console.log(`Err: ${err}`);
  });
  }
  $("select#priority").change(function() {
    $("#priority option:selected").attr("selected", "selected");
    //console.log($("#priority option:selected"));
    var label = $("#priority option:selected").val();
    card.idLabels = label;
    //console.log(card.idLabels);
  });
  $("#calendar").change(function() {
    var duedate = $("#calendar").val();
    card.due = `${duedate} 23:59:00`;
    console.log(duedate);
    console.log(card);
  });
  //Preguntas  desmarcadas
  var description_card = [];
  $(`.pregunta.${variable}`).each(function() {
    if ($(this).prop('checked') == true){
      description_card.push('\n * '+$(this).val()+'\n');
    }
  });
  $('#desc-card').val((description_card.join()).replace(new RegExp(",", "g"), ""));
  //usuarios a etiquetar!
  fetch(`/trellousers/get/trellousers`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    //console.log(data);
    //usuario Logueado
    $("#lis_users").empty();
    var idTrello = $('td.user').attr('data-trellou');
    //console.log(idTrello);
    var userLog = data.filter(tu => tu._id == idTrello);
    //console.log(userLog);
    //Usuario del Dpto de la Auditoria
    var idDpto = datos.depto_id;
    //console.log(idDpto);
    var userDpto = data.filter(tu => tu.dptoUser.tipo == 'a' && tu.dptoUser._id == idDpto);
    //console.log(userDpto);
    // Usuario Developer tag en todas las tarjetas
    var nameDev = 'webdevelopment2becommerce';
    var userDev = data.filter(tu => tu.userName == nameDev);
    //console.log(userDev);
    //Otros datos para la tarjeta
    card.idList = userDpto[0].dptoUser.idList;
    card.linkBoard = userDpto[0].dptoUser.linkBoard;
    card.userAutor = $('td.user').attr('data-user'); //userLog[0].id_user;

    if (data) {
      card.idMembers = `${userLog[0].id_user},${userDpto[0].id_user},${userDev[0].id_user}`;
      var names = [
        `${userLog[0].userName}`,
        `${userDpto[0].userName}`,
        `${userDev[0].userName}`
      ]
      $.each(names, function(key, value) {
          if (key == 2 || key == 5 || key == 7) {
          $("#lis_users").append('<span class="label label-primary">'+names[key]+'</span>&nbsp;\n');
        }else {
          $("#lis_users").append('<span class="label label-primary">'+names[key]+'</span>&nbsp;');
        }
      }); // close each()
    }

  })
  .catch(err => {
    console.log(`Err: ${err}`);
  });
})
//CREAR TARJETA
$('button#btn-save-card').on('click', function() {
  if($("#solicitud_type option:selected").val() == 0 || $("#priority option:selected").val() == 0 ){
    swal("Cancelado", "Hay campos por completar, verifique antes de continuar", "error");
    if($("#solicitud_type option:selected").val() == 0){
      $("#solicitud_type option:selected").css('border','1px solid red');
    }
  }else{
        var user = $('td.user').attr('data-user');
        fetch(`/audits/get/ultimateaudit/${user}/${datos.depto_id}`, { method: 'GET' }) .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }
        }).then(data =>{
          //console.log(data);
          card.audit = data._id
        })
        .catch(err => {
          console.log(`Err: ${err}`);
        });
      //console.log(card);
      //console.log('card isConfirm');
      card.name = $(`input#card-titulo`).val();
      card.desc = 'ASIN PARENT: '+datos.asin+' \n Descripcion: \n'+$(`textarea#desc-card`).val();
      //console.log([$('select#priority').val()]);
      card.urlSource = datos.url;
      console.log(card);
        swal({
          title: "Estas seguro?",
          text: "Quizas quieras verificar bien tus respuestas",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Aceptar",
          cancelButtonText: "Cancelar",
          closeOnConfirm: false,
          closeOnCancel: false
        }, function(isConfirm) {
          if (isConfirm) {

            fetch(`/trello/create/card`, {
                method: 'POST',
                body: JSON.stringify(card),
                headers: {
                  "Content-Type": "application/json"
                },
                credentials: "same-origin"
              })
              .then(response => {
                if (response.ok) {
                  statuscard = true;
                  console.log(response.status);
                  console.log(response);
                  $('#lis_users').empty();
                  document.getElementById("desc-card").value = "";
                  $('#card-titulo').val('');
                  console.log('hide modal');
                  $('#cardModal').hide();
                  swal("Bien hecho!", "Creaste una tarjeta en trello", "success");
                  $('.modal-backdrop').hide();
                  //$('#accordion-'+codigo_q).fadeOut();
                } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }
              })
              .catch(err => {
                console.log(`Err: ${err}`);
              });
            $('#cardModal').modal('hide');
            //limpiar modal siempre que se cierre
            $('#lis_users').empty();
            document.getElementById("desc-card").value = "";
            $('input#card-titulo').val('');
            $("#solicitud_type option#ft").attr("selected", "selected");
            $("#priority option#fp").attr("selected", "selected");

          } else {
            swal("Cancelado", "La tarjeta no se creó", "error");
            $('#lis_users').empty();
            document.getElementById("desc-card").value = "";
            $('input#card-titulo').val('');
            $('#cardModal').modal('hide');
            $("#solicitud_type option#ft").attr("selected", "selected");
            $("#priority option#fp").attr("selected", "selected");
          }
        });


  }

});

//boton close modal Card
$('button#cancel-modal').on('click', function(){
  $('#lis_users').empty();
  document.getElementById("desc-card").value = "";
  $('input#card-titulo').val('');
  $('#cardModal').modal('hide');
  $("#solicitud_type option#ft").attr("selected", "selected");
  $("#priority option#fp").attr("selected", "selected");

});


function obtenerasins(){
setTimeout(function(){
    swal("Bien hecho!", "Guardaste la auditoria", "success");
 }, 1000);

      //===========Obtener ASIN availables
          $('#selectasin').empty();
          $('#openModal').modal('show');
          $('#openModal').modal('handleUpdate')
          //console.log(datos);
          fetch(`/asins/get/audit/${datos.depto_id}_${datos.pais}/brand/${datos.marca}`, { method: 'GET' }) .then(response => {
            if (response.ok) {
              return response.json();
            } else {
              return Promise.reject({
                status: response.status,
                statusText: response.statusText
              })
            }
          }).then(data => {
            //console.log(data);
            datos.idAsin = data._id;
            //console.log(datos);
            $('#spinner').html("");
            $('#spinner').removeClass('loader');
            if(data.length > 0 ){
              $.each(data, function(key, value) {
                $("#selectasin").append(`<p><button type="button" class="list-group-item list-group-item-action">`+
                `<input data-idasin="${data[key]._id}" type="radio" name="marca" class="radio-marca m-r-xs" value="${data[key].asinParent}">`+
                 `<label>${data[key].asinParent} -- SKU: ${data[key].sku} </label></button></p>`);
              }); // close each()
              $(".radio-marca").change(function() {
                datos.asin = $("input[name='marca']:checked").val();
                datos.idAsin = $("input[name='marca']:checked").attr('data-idasin');
                //console.log(datos);
                $("#audit_data").empty();
                $("#audit_data").append('<p>&nbsp;ASIN: <span class="label label-warning-light float-right">'+datos.asin+'</span></p>')

              });
            }else{
              var msj = '<p>No hay asin de esta marca disponibles para realizarle auditoria.</p>';
              $('#selectasin').append(msj);
            }
          }).catch(err => {
            console.log(`Err: ${err}`);
          });
          $('.sweet-alert.showSweetAlert').css('display','none');
          $('.sweet-overlay').css('display','none');

}


$('button#saveaudit').on('click', function(){
  //console.log('le diste a guardar audit');
  /*  if(statuscard == false){
      swal("Cancelado", "Verifica y crea la tarjeta en trello que tienes pendiente", "error");
    }else{*/
    swal({
      title: "Estas seguro?",
      text: "Quizas quieras verificar bien tus respuestas",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Guardar y continuar",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function(isConfirm) {
      if (isConfirm) {

    //============Update Audit
    $(".pregunta").each(function() {
      if ($(this).prop('checked')==false){
        //items evaluados de forma NOoK, se agregan a tarjeta
        var p = $(this).val()
        var tipo = $(this).attr('data-variable');
        generarEvaluacion(evaluacion, tipo, p, false);

      }else{
        //items evaluados de forma OK, no se agregan a tarjeta
        var p = $(this).val()
        var tipo = $(this).attr('data-variable');
        generarEvaluacion(evaluacion, tipo, p, true);

      }
    });
    var totalsi = 0;
      for(var i = 0; i < evaluacion.length; i++){

        for(var a = 0; a < evaluacion[i].result.length; a++){
            //console.log(evaluacion[i].result[a]);
            if(evaluacion[i].result[a].r == false){
              totalsi ++;
            }
        }
      }
    var aud = {
      evaluacion: evaluacion,
      result: (100*totalsi)/totalQ
    }
    var user = $('td.user').attr('data-user');
    //console.log(datos.depto_id);
    //console.log('obtendremos el ultimo audit');
    $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
    $('#spinner').addClass('loader');

    fetch(`/audits/get/ultimateaudit/${user}/${datos.depto_id}`, { method: 'GET' }) .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    }).then(data =>{
      //console.log(data);
      //console.log(data._id);
      //console.log(aud);
      fetch(`/audits/update/audit/${data._id}`, {
          method: "PUT",
          body: JSON.stringify(aud),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(response => {
          if (response.ok) {
            console.log(response.status);
            evaluacion = [];
            //console.log('reiniciamos el arreglo de evaluacion');
            // SAWL TODO
                obtenerasins();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }
        })
        .catch(err => {
          console.log(`Err: ${err}`);
        });
    })


      } else {
        swal("Cancelado", "La auditoria no se actualizó, verifica tus respuestas y guarda tus cambios", "error");

      }
    });

  //  }


})

//MODAL SELEC ASIN

//cerrar modalAsin
$('button#cerrar').on('click', function(){
  $('#openModal').modal('hide');
  location.reload();
});

$('#sigaudit').on('click', function(){
  nextAudit(datos);
  $('#openModal').modal('hide');
});

$('#configaudit').on('click', function(){
  location.reload();

});
