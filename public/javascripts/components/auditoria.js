$(document).ready(function() {
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');

  $(`li.step1`).css('display', 'block');
  $('.page-heading').hide();
  $('#stepper-step-1').addClass('show');
  $('#stepper-step-1').addClass('active');
  $('.btn_NO').attr('disabled', 'disabled').css("background-color", "gray"); //Chequear solo las q estan en este conjunto de preguntas
  $(".pregunta").change(function(){
    var size = $(".pregunta").length
    $(".pregunta").each(function() {
      if ($(this).prop('checked')==false){
        $('.btn_SI').attr('disabled', 'disabled').css("background-color", "gray"); //Chequear solo las q estan en este conjunto de preguntas
        $('.btn_NO').removeAttr('disabled').css("background-color", "#ec4758"); //Chequear solo las q estan en este conjunto de preguntas
      }
    });
  });

  var newaudit = {
    tipo: Number,
    usuario: String,
    link: String
  }
  var pathname = window.location.pathname;
  console.log(pathname);
  var words = pathname.split('/');
  console.log(words);
  console.log(words[words.length - 1]);
  var dpto = words[words.length - 1];
  newaudit.tipo = parseInt(dpto);
  var dptoName = "";
  console.log(dpto);
  switch (dpto) {
    case 1:
      dptoName = "Optimizacion y contenido"
      break;
    case 2:
      dptoName = "Programacion"
      break;
    case 3:
      dptoName = "Marketing"
      break;
    default:
      dptoName = "No seleccionado"
  };

  let actualizarAsin = (idAsin, dpto) => {
    var data = {
      audits: parseInt(dpto)
    };
    console.log(data);
    fetch(`/asins/update/asin/audits/${idAsin}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .catch(err => {
        // TODO:
      });
  }


  //===============Obtener ASIN =================
  fetch(`/asins/get/asins/${dpto}`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {

        return response.json();

      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }

    })
    .then(data => {
      console.log(data);
      console.log(data[0]);
      if (data.length > 0) {
        var asinParent = data[0].asinParent;
        var asin = `https://www.amazon.com/dp/${asinParent}?th=1&psc=1`;
        console.log(asin);
        var iframe = document.getElementById("iframeProduct");
        iframe.setAttribute("src", asin);
        iframe.setAttribute("data-asin", data[0]._id);
        iframe.setAttribute("data-asinparent", asinParent);
        $('#asinparent').append(`<span class="badge badge-primary">${asinParent}</span>`);
        $('#enlaceasin').append(`<a href='${asin}' target="_blank">${asin}</a>`);
        $('#asinmarca').append(`<span class="btn btn-danger">${data[0].brand}</span>`);
        actualizarAsin(data[0]._id, dpto);

        //===============    CREAR AUDITORIA   ==================
        var user = $('.user').attr('data-user');
        newaudit.usuario = user;
        newaudit.link = data[0]._id;
        console.log(newaudit);
        fetch('/audits/create/audit', {
            method: "POST",
            body: JSON.stringify(newaudit),
            headers: {
              "Content-Type": "application/json"
            },
            credentials: "same-origin"
          }).then(response => {
            if (response.ok) {
              console.log(response.status);
              console.log(response);

              sleep(4000);
              $('#spinner').html("");
              $('#spinner').removeClass('loader');

            } else {
              return Promise.reject({
                status: response.status,
                statusText: response.statusText
              })
            }
          })
          .catch(err => {

          })


        //=======================================================


      } else {
        var iframe = document.getElementById("cont_iframe");
        iframe.append('No hay más productos para auditar.');
        window.open("http://localhost:3000/audits/products/amazon", "mywindow");

      }

    })
    .catch(err => {
      // Do something for an error here
    });





  //=======================  json evaluacion  ========================
  var evaluacion = [];

  //====================== MODAL CARD  ======================


  var codigo_q = "";
  var nro = '';
  var card = {
    //necesario para crear card
    name: String,
    desc: String,
    idList: String, //REQUIRED
    idMembers: String, //string de id trellousers separados por coma
    urlSource: String, // url de producto
    // card Control
    linkBoard: String, // id del board donde se creara la tarjeta
    userAutor: String, // id del usuario logueado
    audit: String // id del audit que se esta realizando
  }
  //======== capturando datos para la tarjeta ===============

  $('#exampleModal').on('show.bs.modal', function(event) {
    //triggered the modal
    var button = $(event.relatedTarget) // Button that triggered the modal
    codigo_q = button[0].dataset.q; // Extract info from data-* attributes
    var question = 'Estandar-US--+Tipo de Solicitud'; //TODO
    var linkboard = button[0].dataset.board;
    var idlist = '5d12548bdf13ca2a71798a8b'; //button[0].dataset.idlist;
    var trellouser = button[0].dataset.trellouser;
    var members = '';
    var urlP = $('#iframeProduct').attr('src');
    card.idList = idlist; //REQUIRED
    card.linkBoard = linkboard;
    card.urlSource = urlP;
    card.idLabels = $('select#priority').val();
    // title
    var description_card = [];
    $(".pregunta").each(function() {
      if ($(this).prop('checked')==false){
        description_card.push('\n * '+$(this).val()+'\n');
      }
    });
    console.log('description_card');
    console.log(description_card);
    $('#desc-card').val((description_card.join()).replace(new RegExp(",", "g"), ""));
    // Description
    nro = button[0].dataset.step;

    var user = $('.user').attr('data-user');
    var pathname = window.location.pathname;
    console.log(pathname);
    var words = pathname.split('/');
    console.log(words);
    console.log(words[words.length - 1]);
    var dpto = parseInt(words[words.length - 1]);
    // obtener id del audit, obtener usuario de trelloU, crear objeto card
    fetch(`http://localhost:3000/audits/get/ultimateaudit/${user}/${dpto}`, { method: 'GET' }) .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      }).then(data => {
        console.log(data._id);
        card.audit = data._id;
        card.userAutor = data.usuario;

        fetch(`/trellouser/get/trellousers`, { method: 'GET' }) .then(response => {
            if (response.ok) {
              return response.json();
            } else {
              return Promise.reject({
                status: response.status,
                statusText: response.statusText
              })
            }
          }).then(data => {
            console.log(data);
            console.log(trellouser);
            var pathname = window.location.pathname;
            console.log(pathname);
            var words = pathname.split('/');
            console.log(words);
            console.log(words[words.length - 1]);
            var dpto = words[words.length - 1];
            dptou = parseInt(dpto);
            var userDpto = data.filter(tu => tu.dptoUser == dptou);
            console.log(userDpto);
            var trelloU = data.filter(tu => tu._id == trellouser);
            var userdev = data.filter(tu => tu.dptoUser == 0);
            console.log(userdev);
            console.log(trelloU[0].id_user);
            // TODO: agregar a members ${userDpto[0].id_user}, quitar ${userdev[0].id_user}
            members = `${trelloU[0].id_user},${userdev[0].id_user}`;
            var membersTags = [`${trelloU[0].userName}`, `${userDpto[0].userName}`, `${userdev[0].userName}`];
            // card.idMembers = members;
            console.log(card);

            for (var i = 0; i < membersTags.length; i++) {
              var usertrello = `<span class="label label-primary">${membersTags[i]}</span>`
              $('#lis_users').append(usertrello);
            }

          })
          .catch(err => {
            // Do something for an error here
          });


      })
      .catch(err => {
        // Do something for an error here
      });



    //=================================================
    var modal = $(this);
    $('input#card-titulo').val(JSON.stringify(question.replace(new RegExp("  ", "g"), "").replace(new RegExp("\n", "g"), "")));
    modal.show();
  });

  $('#exampleModal').on('hide.bs.modal', function(event) {
    $('#lis_users').empty();
    document.getElementById("desc-card").value = "";
    var modal = $(this);
    $('input#card-titulo').val('');
    modal.hide();
  });

  //===================guardar tarjeta =====================

  $('button#btn-save-card').on('click', function() {
    var item = {
      variable: String,
      dpto: Number,
      pregunta: String,
      respuesta: Number

    }
    //======= info card
    var asin = $('#iframeProduct').attr('data-asinparent');
    card.desc = 'ASIN PARENT: '+asin+' \n Descripcion: \n'+$(`textarea#desc-card`).val();
    card.idLabels = [$('select#priority').val()];
    card.name = $(`input#card-titulo`).val()+'-'+$(`select#solicitud_type`).val();
    console.log('****************************');
    console.log(card.desc);
    console.log(card.name);
    console.log(card);

    //============CREAR TARJETA ======
    swal({
        title: "Estas seguro?",
        text: "Quizas quieras verificar bien tu respuesta",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          //======Respuesta Negativa =======
          console.log('respuesta no = 0');
          var question = 'Estandar-US-+Tipo de Solicitud'; //TODO
          item.variable = question.split('-')[0].toLowerCase().trim();
          item.pregunta = question.trim();
          item.respuesta = 0;
          item.dpto = parseInt($('#dpto-audit').attr('data-dpto'));
          console.log(item);
          evaluacion.push(item);
          console.log(evaluacion);
          $(`button.btn_SI[data-q=${codigo_q}]`).attr('disabled', 'disabled');
          $(`button.btn_NO[data-q=${codigo_q}]`).attr('disabled', 'disabled');
          var answericon = $(`a#${codigo_q} div.answericon`);
          answericon.append('<i class="fas fa-times-circle"></i>');
          $(`div.panel-heading[data-q=${codigo_q}]`).attr('data-check', 'true');
          verificaPreguntas(item.variable, nro);
          //==========================================
          console.log(card);

          return fetch(`/trello/create/card`, {
              method: 'POST',
              body: JSON.stringify(card),
              headers: {
                "Content-Type": "application/json"
              },
              credentials: "same-origin"
            })
            .then(response => {
              if (response.ok) {
                console.log(response.status);
                console.log(response);
                $('#lis_users').empty();
                document.getElementById("desc-card").value = "";
                $('#card-titulo').val('');
                console.log('hide modal');
                $('#exampleModal').hide();
                $(`button.btn_SI[data-q=${codigo_q}]`).attr('disabled', 'disabled');
                $(`button.btn_NO[data-q=${codigo_q}]`).attr('disabled', 'disabled');
                $(`div.panel-heading[data-q=${codigo_q}]`).attr('data-check', 'true');
                swal("Bien hecho!", "Creaste una tarjeta en trello", "success");
                $('.modal-backdrop').hide();
                $('#accordion-'+codigo_q).fadeOut();
              } else {
                return Promise.reject({
                  status: response.status,
                  statusText: response.statusText
                })
              }
            })

        } else {
          swal("Cancelado", "La tarjeta no se creó", "error");
          $('#lis_users').empty();
          document.getElementById("desc-card").value = "";
          $('input#card-titulo').val('');
          $('#exampleModal').modal('hide');
        }
      });


  });


  //=================== CREANDO LA AUDITORIA ===========

  let verificaPreguntas = (variable, nro) => {
    console.log('verificando pregunta');
    console.log(variable);
    console.log(nro);
    var items = $(`[data-tipoq=${variable}]`);
    var todasP = $('div.pregunta');
    var pCheck = $(`div.pregunta[data-check='true']`);
    var checked = $(`[data-tipoq=${variable}][data-check='true']`);
    console.log(items.length);
    console.log(checked);
    if (checked.length == items.length) {
      var next = parseInt(nro) + 1;
      $(`li.step${next}`).css('display', 'block');
      $(`li.step${nro}`).css('display', 'none');
      $(`#stepper-step-${next}`).addClass('show');
      $(`#stepper-step-${next}`).addClass('active');
      $(`#stepper-step-${nro}`).removeClass('show');
      $(`#stepper-step-${nro}`).removeClass('active');
    }
    if (pCheck.length === todasP.length) {
      $(`button.next`).removeAttr('disabled');
      $(`button.next`).css('display', 'block');
      $(`button.stop`).removeAttr('disabled');
      $(`button.stop`).css('display', 'block');
    }

  }



  $('button.btn_SI').on('click', function() {
    var item = {
      variable: String,
      dpto: Number,
      pregunta: String,
      respuesta: Number

    }
    console.log('respuesta si = 1');
    console.log($(this));
    var idp = $(this).attr('data-q');

    var nro = $(this).attr('data-step');
    console.log($(`a#${idp}`));
    var question = 'Estandar-US--+Tipo de Solicitud'; //TODO
    item.variable = question.split('-')[0].toLowerCase().trim();
    item.pregunta = question.trim();
    item.respuesta = 1;
    item.dpto = parseInt($('#dpto-audit').attr('data-dpto'));
    console.log(item);
    evaluacion.push(item);
    console.log(evaluacion);
    $(`button.btn_SI[data-q=${idp}]`).attr('disabled', 'disabled');
    $(`button.btn_NO[data-q=${idp}]`).attr('disabled', 'disabled');
    var answericon = $(`a#${idp} div.answericon`);
    answericon.append('<i class="fas fa-check-circle"></i>');
    $(`div.panel-heading[data-q=${idp}]`).attr('data-check', 'true');
    verificaPreguntas(item.variable, nro);
    $('#accordion-'+idp).fadeOut();
  });


  // actualizar audit y cargar el nuevo audit

  let actualizarAudit = (user, dpto, audit) => {
    return fetch(`http://localhost:3000/audits/get/ultimateaudit/${user}/${dpto}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log('get id audit');
        console.log(data);
        console.log(data._id);
        $('#iframeProduct').attr('data-auditid', data._id);

        fetch(`http://localhost:3000/audits/update/audit/${data._id}`, {
            method: "PUT",
            body: JSON.stringify(audit),
            headers: {
              "Content-Type": "application/json"
            },
            credentials: "same-origin"
          }).then(response => {
            if (response.ok) {
              console.log(response.status);

            } else {
              return Promise.reject({
                status: response.status,
                statusText: response.statusText
              })
            }
          })
          .catch(err => {

          })

      })
      .catch(err => {
        // Do something for an error here
      });

  }



  //=================================

  $('button#next_audit').on('click', function() {
    var auditactual = {
      evaluacion: evaluacion //array de objetos preguntas y respuestas  {variable, dpto,  pregunta, respuesta}
    }

    swal({
        title: "Estas seguro?",
        text: "Una vez empieces la auditoria debes culminarla!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, continuar!",
        cancelButtonText: "No, guardar y salir",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          var pathname = window.location.pathname;
          console.log(pathname);
          var words = pathname.split('/');
          var dpto = words[words.length - 1];
          var user = $('.user').attr('data-user');
          actualizarAudit(user, dpto, auditactual);
          setTimeout(function() {
            location.reload();
          }, 3000);
        } else {
          var pathname = window.location.pathname;
          console.log(pathname);
          var words = pathname.split('/');
          var dpto = words[words.length - 1];
          var user = $('.user').attr('data-user');
          actualizarAudit(user, dpto, auditactual);
          setTimeout(function() {
            location.href = `http://localhost:3000/audits/products/amazon`;
          }, 3000);
        }
      });



  });

  $('button#stop_audit').on('click', function() {
    var auditactual = {
      evaluacion: evaluacion //array de objetos preguntas y respuestas  {variable, dpto,  pregunta, respuesta}
    }

    swal({
  title: "Estas seguro?",
  text: "No continuaras realizando auditorias!",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Acepto!",
  cancelButtonText: "Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm) {
  if (isConfirm) {

        var pathname = window.location.pathname;
        console.log(pathname);
        var words = pathname.split('/');
        var dpto = words[words.length - 1];
        var user = $('.user').attr('data-user');
        actualizarAudit(user, dpto, auditactual);
        setTimeout(function() {
          location.href = `http://localhost:3000/audits/products/amazon`;
        }, 3000);

    swal("Excelente", "Puedes continuar configurando una auditoria diferente", "success");
  } else {
    swal("Cancelado", "Puedes escoger una opción diferente", "error");
  }
});


  });



});

function sleep(milisegundos) {
  console.log('ejecutando sleep');
  var comienzo = new Date().getTime();
  while (true) {
    if ((new Date().getTime() - comienzo) > milisegundos)
      break;
  }
}
