/*GENERAL*/
//================== Functions ================================
$(document).ready(function(){

  var oTable = '';
  var id = '';
  var $this = '';
  var newuser = {}
  var marcas = [];


//=========== obtener datos del usuario al abrir el modal ====
let activarDesactivar = (opcion, id) =>{
  console.log(id);
  console.log(opcion);
  if(opcion == 'delete'){
    swal({
        title: "Estas seguro?",
        text: "Una vez desactivado, este usuario no estará disponible",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si , Desactivar!",
        closeOnConfirm: false
      },
      function(isConfirm){
        if (isConfirm) {
          return fetch(`/users/desactivar/user/${id}`, {
              method: 'delete'
            })
            .then(response => {
              if (response.ok) {
                location.reload();
              } else {
                return Promise.reject({
                  status: response.status,
                  statusText: response.statusText
                })
              }
            })
          swal("Poof! Usuario desactivado", {
            icon: "success",
          });
        } else {
          swal("El usuario no se ha desactivado!");
        }
      });
  }
  if(opcion == 'activate'){
      console.log('activadonos');
      swal({
          title: "Estas seguro?",
          text: "Una vez activado, este usuario estará disponible",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si , Activar!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            return fetch(`/users/activate/user/${id}`, {
                method: 'delete'
              })
              .then(response => {
                if (response.ok) {
                  location.reload();
            } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }
              })
            swal("Poof! Usuario activado", {
              icon: "success",
            });
          } else {
            swal("El usuario ha sido activado!");
          }
        });
  }
}


let obtenerDatosUser = (iduser) => {
  console.log(iduser);
  if(iduser != '' || iduser != undefined){
    fetch(`/users/get/user/${iduser}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        var brandis = data.marcas;
        if(data.status == 1){
          $('input#passuser').val(data.password);
          $('input#emailData').val(data.email);
          $(`option#${data.tipo}`).attr('selected', 'selected');
          if(data.trellouser.userName != '' || data.trellouser.userName != undefined){
                $(`option#${data.trellouser.userName}`).attr('selected', 'selected');
                $('#trellousers').attr('disabled', 'disabled');
          }else {
                $('#trellousers').removeAttr('disabled');
          }
          $('#my-select').multiSelect('deselect_all');
          brandis.forEach(function(element){
            console.log(element);
                $('#my-select').multiSelect('select', element);
          })
      }else{
          swal("Ups!", "Este usuario esta desactivado y no puede ser editado", "warning");
          location.reload();
        }

      })
      .catch(err => {
        // Do something for an error here
      })
  }else {
    swal("Ups!", "Debes seleccionar un usuario", "warning");
  }


}


// ========== fin de obtener datos

  /* MODAL */
  $('#userModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    console.log(button);
    var accion = button[0].dataset.title; // Extract info from data-* attributes
    if(accion == 'crear'){
      $('span.marcas').css('display', 'none');
      $('label#brand-selected').css('display', 'none');
      $('#btn-create-user').show();
      $('#btn-edit-user').hide();
    }
    if(accion == 'editar'){
      console.log('editar');
      $('span.marcas').css('display', 'block');
      $('label#brand-selected').css('display', 'block');
      $('#btn-create-user').hide();
      $('#btn-edit-user').show();

      $('#emailData').attr('disabled', 'disabled');

      var iduser = button[0].dataset.id;
      id = button[0].dataset.id;
      obtenerDatosUser(iduser);

    }
    var modal = $(this);
    modal.find('.modal-title').text(`${accion.toUpperCase()} Usuario`);
    modal.show();
  });
  $('#userModal').on('hide.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    console.log(button);
      $("input#emailData").text('');
      $("input#passuser").text('');
      var modal = $(this);
      modal.hide();
  });

  /*dataTables begin*/
  fetch(`/users/get/users`,
     {
        method: 'GET'
       })
       .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    oTable = $('#table-data').DataTable({
      data: data,
      language: {
        "emptyTable":     "No hay datos disponibles",
        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
        "search":         "Buscar:",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        }
      },
      columns: [
        { data: 'email', "defaultContent":"-" },
        { data: 'tipo', "defaultContent":"-" },
        { data: 'trellouser.userName', "defaultContent":"-" },
        { data: 'marcas', "defaultContent":"-" },
        { data: 'status',
        "defaultContent":"-",
        "render": function(data, type, row, meta){
          console.log(type);
          console.log(meta);
          if(type === 'display'){
            if (data == '1') {
              data = '<span class="badge badge-primary badge-pill label label-success">Active</span>';
            }else{
              $(`button.edit[data-id = ${row._id}]`).attr('disabled', 'disabled');
              $(`button.delete[data-id = ${row._id}]`).addClass('activate');
              $(`button.activate[data-id = ${row._id}]`).removeClass('delete');
              $(`button.activate[data-id = ${row._id}]`).removeClass('btn-primary');
              $(`button.activate[data-id = ${row._id}]`).addClass('btn-danger');
              $(`button.activate[data-id = ${row._id}]`).html('<span aria-hidden="true"><i class="fas fa-trash-restore-alt"></i></span>');
              data = '<span class="badge badge-primary badge-pill label label-danger"> Inactive </span>';
            }
          }
          return data;
        }

      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button  type="button" data-title='editar'  data-toggle='modal' data-target='#userModal' class="btn btn-primary option edit" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-edit"></i></span>`+
            `</button>`

        return boton;
      }
      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button type="button" name="delete" class="btn btn-primary option delete" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-trash-alt"></i></span>`+
            `</button>`

        return boton;
      }
      }



    ],
    order: [[ 0, "desc" ]]
    });


//desactivar o activar usuario==================
//seleccionar una fila
$('button.option').on( 'click', function () {
  console.log($(this).attr('data-id'));
  id = $(this).attr('data-id');
if ($(this).hasClass("delete")){
  console.log('delete');
  var opcion = 'delete';
  activarDesactivar(opcion, id);
}else if($(this).hasClass("activate")){
  console.log('activate');
  var opcion = 'activate';
  activarDesactivar(opcion, id);
}
});
  });
  /*end datatables*/


/*Obtener usuarios de trello*/
fetch(`/trellousers/get/trellousers`, {
    method: 'GET'
  })
  .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }

  })
  .then(data => {
    data.forEach(function(element) {
      var id = element._id
      var name = element.userName;
      var option = '<option id="'+ name+'" value="' + id + '">' + name + '</option>';
      $('#trellousers').prepend(option);

    });

  })
  .catch(err => {
    // Do something for an error here
  })


/*Fin de usuarios de trello*/




/*Mostrar ocultar contraseña*/
$('button#show_password').on('click',  function (){
		var cambio = document.getElementById("passuser");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	});
/*fin ocultar mostrar contraseña*/

/*Multiselect */
$('#my-select').multiSelect({
  selectableHeader: "<div class='custom-header'>Selecciona una marca</div>",
  selectionHeader: "<div class='custom-header'>Marcas Asignadas</div>",

  afterSelect: function(values){
    console.log(values);
      marcas.push(`${values}`);
      console.log(marcas);
      console.log(newuser);
  },
  afterDeselect: function(values){
      console.log(values);
      var index = marcas.indexOf(values[0]);
      if(index > -1){
         marcas.splice(index, 1);
      }
  }
});

/* Fin multiselect*/


/*Actualizando Data*/
  $("input#emailData").change(function(){
    newuser.email = $("input#emailData").val();
    console.log(newuser);
  });

$("input#passuser").change(function(){
  newuser.password = $("input#passuser").val();
  console.log(newuser);
});
$("select[name=tipousers]").change(function() {
  $("#tipoData option:selected").attr("selected", "selected");
  newuser.tipo = $("#tipoData option:selected").val();
  console.log(newuser);
});
$("select[name=trellousers]").change(function() {
  $("#trellousers option:selected").attr("selected", "selected");
    newuser.trellouser = $("#trellousers option:selected").val();
    console.log(newuser);
});
$("select[name=selectbrand]").change(function() {
  console.log('selectbrand');
  newuser.marcas =marcas;
    console.log(newuser);
});


/*CRUD METHODS*/

  //=================Crear Usuario ===========================

  $('button#btn-create-user').on('click', function(){
    console.log('newuser');
    console.log(newuser);
    var comodin = false;
    for (let i in newuser) {
      console.log(newuser[i]);
      if(newuser[i] == '' || newuser[i] == null || newuser[i] == undefined) {
            console.log(`${newuser[i]} vacio`);
            swal({
              title: "Verifica",
              text: "Todos los campos deben estar llenos",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          }else{
            comodin = true;
          }

      }

if(comodin === true){
  fetch('/users/create/user', {
      method: "POST",
      body: JSON.stringify(newuser),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(response => {
      if (response.ok) {
        console.log(response.status);
        console.log(response);
        $('.modal').hide();
        swal("Bien hecho!", "Creaste un nuevo usuario", "success");
        location.reload();

      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    })
    .catch(err => {
      if (err) {
        swal({
          title: "Ups!",
          text: "Ocurrio un problema, el usuario no se creo",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
      }
    })

}
  });
  //===========fin de create =====================


    //==================Editar Usuario =======================================


    $('button#btn-edit-user').on('click', function() {
      console.log('newuser');
      console.log(newuser);
      console.log(id);
      fetch(`/users/update/user/${id}`, {
        method: "PUT",
        body: JSON.stringify(newuser),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
      }).then(response => {
        if (response.ok) {
          $('.modal').hide();
          swal("Bien Hecho", "El usuario ha sido editado", "success");
         location.reload();

        } else {
          console.log('limpia iduser');
          id = '';
          console.log(id);

          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })

    });




});

function sleep(milisegundos) {
  console.log('ejecutando sleep');
  var comienzo = new Date().getTime();
  while (true) {
    if ((new Date().getTime() - comienzo) > milisegundos)
      break;
  }
}
