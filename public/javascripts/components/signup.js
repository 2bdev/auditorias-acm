var newuser = {}

  $('button#show_password').on('click',  function (){
  		var cambio = document.getElementById("passuser");
  		if(cambio.type == "password"){
  			cambio.type = "text";
  			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
  		}else{
  			cambio.type = "password";
  			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
  		}
  	});

    /*Actualizando Data*/
      $("input#email").change(function(){
        newuser.email = $("input#email").val();
        console.log(newuser);
      });

    $("input#passuser").change(function(){
      newuser.password = $("input#passuser").val();
      console.log(newuser);
    });
    $("select[name=tipousers]").change(function() {
      $("#tipoData option:selected").attr("selected", "selected");
      newuser.tipo = $("#tipoData option:selected").val();
      console.log(newuser);
    });





      $('button#connectLink').on('click', function(){
        console.log('newuser');
        console.log(newuser);
        var comodin = false;
        for (let i in newuser) {
          console.log(newuser[i]);
          if(newuser[i] == '' || newuser[i] == null || newuser[i] == undefined) {
                console.log(`${newuser[i]} vacio`);
                swal({
                  title: "Verifica",
                  text: "Todos los campos deben estar llenos",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
              }else{
                comodin = true;
              }

          }

    if(comodin === true){
      fetch('/users/create/user', {
          method: "POST",
          body: JSON.stringify(newuser),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(response => {
          if (response.ok) {
            console.log(response.status);
            console.log(response);
            $('.modal').hide();
            swal("Bien hecho!", "Creaste un nuevo usuario", "success");
            

          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }
        })
        .catch(err => {

          if (err) {
            console.log(`Err: ${err}`);
            swal({
              title: "Ups!",
              text: "Ocurrio un problema, el usuario no se creo",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          }
        })

    }
      });
