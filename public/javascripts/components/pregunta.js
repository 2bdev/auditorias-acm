var id = '';
var datos = {};
console.log(datos);
$(document).ready(function(){

  //=========== obtener datos del usuario al abrir el modal ====
  let activarDesactivar = (opcion, id) =>{
    console.log(id);
    console.log(opcion);
    if(opcion == 'delete'){
      swal({
          title: "Estas seguro?",
          text: "Una vez desactivado, esta pregunta no estará disponible",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si , Desactivar!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            return fetch(`/questions/delete/question/${id}`, {
                method: 'delete'
              })
              .then(response => {
                if (response.ok) {
                  location.reload();
                } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }
              })
            swal("Poof! Pregunta desactivado", {
              icon: "success",
            });
          } else {
            swal("La pregunta no se ha desactivado!");
          }
        });
    }
    if(opcion == 'activate'){
        console.log('activadonos');
        swal({
            title: "Estas seguro?",
            text: "Una vez activado, esta pregunta estará disponible",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si , Activar!",
            closeOnConfirm: false
          },
          function(isConfirm){
            if (isConfirm) {
              return fetch(`/questions/activate/question/${id}`, {
                  method: 'delete'
                })
                .then(response => {
                  if (response.ok) {
                    location.reload();
              } else {
                    return Promise.reject({
                      status: response.status,
                      statusText: response.statusText
                    })
                  }
                })
              swal("Poof! Pregunta activada", {
                icon: "success",
              });
            } else {
              swal("La pregunta ha sido activada!");
            }
          });
    }
  }


  /* MODAL */
  $('#exampleModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    console.log(button);
    var accion = button[0].dataset.title; // Extract info from data-* attributes
    if(accion == 'crear'){
      $('#btn-save-q').show();
      $('#btn-edit-q').hide();}
    var modal = $(this);
    modal.find('.modal-title').text(`${accion.toUpperCase()} PREGUNTA`);
    modal.show();
  });

//============= fin modal ====================


// ====== dataTables ======================
  fetch(`/questions/get/questions`,
     {
        method: 'GET'
       })
       .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    oTable = $('#table-data').DataTable({
      data: data,
      language: {
        "emptyTable":     "No hay datos disponibles",
        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
        "search":         "Buscar:",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        }
      },
      columns: [
        { data: 'contentQuestion', "defaultContent":"-" },
        { data: 'typeQuestion', "defaultContent":"-" },
        { data: 'dptoQuestion.name', "defaultContent":"-" },
        { data: 'statusQuestion',
        "defaultContent":"-",
        "render": function(data, type, row, meta){
          console.log(type);
          console.log(meta);
          if(type === 'display'){
            if (data == '1') {
              data = '<span class="badge badge-primary badge-pill label label-success">Active</span>';
            }else{
              $(`button.edit[data-id = ${row._id}]`).attr('disabled', 'disabled');
              $(`button.delete[data-id = ${row._id}]`).addClass('activate');
              $(`button.activate[data-id = ${row._id}]`).removeClass('delete');
              $(`button.activate[data-id = ${row._id}]`).removeClass('btn-primary');
              $(`button.activate[data-id = ${row._id}]`).addClass('btn-danger');
              $(`button.activate[data-id = ${row._id}]`).html('<span aria-hidden="true"><i class="fas fa-trash-restore-alt"></i></span>');
              data = '<span class="badge badge-primary badge-pill label label-danger"> Inactive </span>';
            }
          }
          return data;
        }
      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button type="button" class="btn btn-primary option delete" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-trash-alt"></i></span>`+
            `</button>`

        return boton;
      }
      }

    ],
    order: [[ 0, "desc" ]]
    });

    //seleccionar una fila
    $('button.option').on( 'click', function () {
      console.log($(this).attr('data-id'));
      id = $(this).attr('data-id');
    if ($(this).hasClass("delete")){
      console.log('delete');
      var opcion = 'delete';
      activarDesactivar(opcion, id);
    }else if($(this).hasClass("activate")){
      console.log('activate');
      var opcion = 'activate';
      activarDesactivar(opcion, id);
    }
    });

  });



  //actualizar data
  $("select[name=typeQuestion]").change(function(){
    $("#tipoData option:selected").attr("selected", "selected");
    datos.typeQuestion = $("#tipoData option:selected").val()
    console.log(datos.typeQuestion);
    console.log(datos);
  });
  $("input#questionData").change(function(){
    datos.contentQuestion = $("input#questionData").val();
    console.log(datos);
  });
  $("select[name=dptoQuestion]").change(function(){
    $("#dptoQuestion option:selected").attr("selected", "selected");
    datos.dptoQuestion = $("#dptoQuestion option:selected").val();
    console.log(datos);
    fetch(`/departamentos/get/variables/dpto/${datos.dptoQuestion}`, { method: 'GET' }) .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    }).then(data => {
      $('#i1').css('display','block');
      console.log(data);
      $.each(data, function(key, value) {
        $("#tipoData").append('<option id="' + value+ '" value="' + value+ '">' + value+ '</option>');
      }); // close each()



    });

  });
  /*Add Departamentos*/
  fetch(`/departamentos/get/departamentos`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    $.each(data, function(key, value) {
      $("#dptoQuestion").append('<option data-id="' + value._id + '" value="' + value._id + '">' + value.name + '</option>');
    }); // close each()



  });
});


$('td#update-q').on('click', function(){
  var id = $(this).attr('data-q');
  console.log(id);
  fetch(`/questions/get/question/${id}`, {
    method: 'GET'
  })
    .then(response => {
      if(response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }

    })
    .then(data => {
      var pregunta = data.contentQuestion;
      var tipo = data.typeQuestion;
      var depto = data.dptoQuestion;
      var id = data._id;
      $('#id_question').val(id);
      $('input#questionData').val(pregunta);
      $('select[name=dptoQuestion] option[value="'+depto._id+'"]').attr('selected', 'selected');
      if (tipo == 1 || tipo == 'titulo') {
        $('#tipoData option[value="1"]').attr('selected', 'selected');
      }else {
        $('#tipoData option[value="2"]').attr('selected', 'selected');
      }
      $('button#btn-edit-q').attr('data-q', id);
      console.log(data)
    })
    .catch(err => {
      // Do something for an error here
    })

});



$('button#btn-edit-q').on('click', function(){
  var id = $('#id_question').val();
  var url = '/questions/update/question/'+id
fetch(url, {
    method: "PUT",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
    }).then(response => {
      if (response.ok) {
        console.log(response.status);
        console.log(response);
        $('.modal').hide();
        swal("Bien Hecho!", "Editaste una pregunta!", "success");
        location.reload();

      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    })

});

//=================Crear Pregunta =====================================

$('button#btn-save-q').on('click', function(){
  console.log(datos);
fetch(`/questions/create/question`, {
    method: "POST",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
    }).then(response => {
      if (response.ok) {
        console.log(response.status);
        console.log(response);
        $('.modal').hide();
        swal("Bien Hecho!", "Creaste una nueva pregunta", "success");
        location.reload();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    })

});




//=================Eliminar Pregunta =====================================

$('td#delete-q').on('click', function (){
  var id = $(this).attr('data-q');
  console.log(id);
  console.log('td#delete-q');
  var url = '/questions/delete/question/'+id;
  swal({
    title: "Estas seguro?",
    text: "Una vez eliminado, esta pregunta se desactivará!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  },
  function(isConfirm){
    if (isConfirm) {
      return fetch(url, {
        method: 'delete'
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          console.log(response);
          location.reload();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })
      swal("Poof! Pregunta eliminada", {
        icon: "success",
      });
    } else {
      swal("Cancelled", "Tu pregunta esta a salvo :)", "error");
    }
  });
})


$('td#activar-q').on('click', function (){
  var id = $(this).attr('data-q');
  console.log(id);
  console.log('td#activar-q');
  var url = '/questions/activate/question/'+id;
  swal({
    title: "Estas seguro?",
    text: "Una vez activada, esta pregunta estará disponible!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  },
  function(isConfirm){
    if (isConfirm) {
      return fetch(url, {
        method: 'PUT'
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          console.log(response);
          location.reload();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })
      swal("Poof! Pregunta activada", {
        icon: "success",
      });
    } else {
      swal("Cancelled", "Tu pregunta esta a salvo :)", "error");
    }
  });
})
