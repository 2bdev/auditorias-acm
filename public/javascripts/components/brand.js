$('#exampleModal').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  console.log(button);
  var recipient = button[0].dataset.title; // Extract info from data-* attributes
  console.log(recipient);
  if(recipient == 'Crear Marca'){
    $('#btn-save-q').show();
    $('#btn-edit-q').hide();
  }else{
    $('#btn-save-q').hide();
    $('#btn-edit-q').show();
  }
  var modal = $(this)
  modal.find('.modal-title').text(recipient)
  modal.show();
});

//==================Editar Marca =======================================
var datos = {};

$(document).ready(function(){

  //=========== obtener datos del usuario al abrir el modal ====
  let activarDesactivar = (opcion, id) =>{
    console.log(id);
    console.log(opcion);
    if(opcion == 'delete'){
      swal({
          title: "Estas seguro?",
          text: "Una vez desactivado, esta marca no estará disponible",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si , Desactivar!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            return fetch(`/brands/desactivar/brand/${id}`, {
                method: 'delete'
              })
              .then(response => {
                if (response.ok) {
                  location.reload();
                } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }
              })
            swal("Poof! Marca desactivada", {
              icon: "success",
            });
          } else {
            swal("La marca no se ha desactivado!");
          }
        });
    }
    if(opcion == 'activate'){
        console.log('activadonos');
        swal({
            title: "Estas seguro?",
            text: "Una vez activado, esta marca estará disponible",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si , Activar!",
            closeOnConfirm: false
          },
          function(isConfirm){
            if (isConfirm) {
              return fetch(`/brands/activate/brand/${id}`, {
                  method: 'delete'
                })
                .then(response => {
                  if (response.ok) {
                    location.reload();
              } else {
                    return Promise.reject({
                      status: response.status,
                      statusText: response.statusText
                    })
                  }
                })
              swal("Poof! Marca activada", {
                icon: "success",
              });
            } else {
              swal("La marca ha sido activada!");
            }
          });
    }
  }


  /*dataTables begin*/
  fetch(`/brands/get/brands`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    var oTable = $('#table').DataTable({
      data: data,
      columns: [
        { data: 'name', "defaultContent":"-" },
        { data: 'shortName', "defaultContent":"-" },
        { data: 'status',
        "defaultContent":"-",
        "render": function(data, type, row, meta){

          if(type === 'display'){
            if (data == '1') {
              data = '<span class="badge badge-primary badge-pill label label-success">Active</span>';
            }else{
              $(`button.delete[data-id = ${row._id}]`).addClass('activate');
              $(`button.activate[data-id = ${row._id}]`).removeClass('delete');
              $(`button.activate[data-id = ${row._id}]`).removeClass('btn-primary');
              $(`button.activate[data-id = ${row._id}]`).addClass('btn-danger');
              $(`button.activate[data-id = ${row._id}]`).html('<span aria-hidden="true"><i class="fas fa-trash-restore-alt"></i></span>');
              data = '<span class="badge badge-primary badge-pill label label-danger"> Inactive </span>';
            }
          }
          return data;
        }
      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button type="button" class="btn btn-primary option delete" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-trash-alt"></i></span>`+
            `</button>`

        return boton;
      }
      }
      ]
    });

    //seleccionar una fila
    $('button.option').on( 'click', function () {
      console.log($(this).attr('data-id'));
      id = $(this).attr('data-id');
    if ($(this).hasClass("delete")){
      console.log('delete');
      var opcion = 'delete';
      activarDesactivar(opcion, id);
    }else if($(this).hasClass("activate")){
      console.log('activate');
      var opcion = 'activate';
      activarDesactivar(opcion, id);
    }
    });


  });
  /*end datatables*/
  $("input#name").change(function(){
    datos.name = $("input#name").val();
    console.log(datos);
  });
  $("input#shortname").change(function(){
    datos.shortName = $("input#shortname").val();
    console.log(datos);
  });
});

//=================Crear Pregunta =====================================
$('button#btn-save-q').on('click', function(){
  console.log(datos);
  var name = $("input#name").val();
  var short = $("select#shortname").val();

if(name != '' && short != ''){
  fetch(`/brands/create/brand`, {
    method: "POST",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  }).then(response => {
    if (response.ok) {
      console.log(response.status);
      console.log(response);
      $('.modal').hide();
      swal("Bien Hecho!", "Creaste una nueva marca", "success");
      location.reload();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  })
}else{
    swal("Falta Campos por completar!", "Recuerda asignar un nombre a la marca y un nombre corto que la identifique en toda la aplicacion", "warning");
  }


});
