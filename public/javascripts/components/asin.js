$(document).ready(function(){
  /*dataTables begin*/
  fetch(`/asins/get/asins`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    var oTable = $('#table').DataTable({
      data: data,
      columns: [
        { data: 'asinParent', "defaultContent":"-" },
        { data: 'sku', "defaultContent":"-" },
        { data: 'brand', "defaultContent":"-" },
        { data: 'country', "defaultContent":"US" },
        { data: 'status',
          "defaultContent":"-",
          "render": function(data, type, row, meta){
            if(type === 'display'){
              if (data == '1') {
                data = '<span class="badge badge-primary badge-pill label label-success">Active</span>';
              }else{
                data = '<span class="badge badge-primary badge-pill label label-danger"> Inactive </span>';
              }
            }
            return data;
          }
        },
        // {
        //   "targets": -1,
        //   "data": null,
        //   "defaultContent": "<button class='btn btn-primary update-q'><i class='far fa-edit'></i><span>Editar</span></button>"
        // },
        // {
        //   "targets": -1,
        //   "data": null,
        //   "defaultContent": "<button class='btn btn-primary delete-q'><i class='fas fa-trash-alt'></i><span>Eliminar</span></button>"
        // }
      ],
      order: [[ 0, "desc" ]]
    });
  });
  /*end datatables*/
});
