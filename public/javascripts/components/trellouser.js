/*GENERAL*/
/* MODAL */
$('#userModal').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  console.log(button);
  var accion = button[0].dataset.title; // Extract info from data-* attributes
  if(accion == 'crear'){
    $('input#emailData').removeAttr('disabled');
    $('#btn-create-user').show();
    $('#btn-edit-user').hide();
  }else{
    $('input#emailData').attr('disabled', 'disabled');
    $('#btn-create-user').hide();
    $('#btn-edit-user').show();
  }
  var modal = $(this);
  modal.find('.modal-title').text(`${accion.toUpperCase()} Usuario`);
  modal.show();
});


//================== Functions ================================
$(document).ready(function(){
  var oTable = '';
  var id = '';
  var $this = '';
  var trellouser = {
    email: String,
    userName: String,
    id_user: String,
    dptoUser: String
  }
  /*dataTables begin*/
  fetch(`/trellousers/get/trellousers`,
     {
        method: 'GET'
       })
       .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    oTable = $('#table-data').DataTable({
      data: data,
      language: {
        "emptyTable":     "No hay datos disponibles",
        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
        "search":         "Buscar:",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        }
      },
      columns: [
        { data: '_id', "defaultContent":"-"},
        { data: 'email', "defaultContent":"-" },
        { data: 'id_user', "defaultContent":"-" },
        { data: 'userName', "defaultContent":"-" },
        { data: 'dptoUser.name', "defaultContent":"-" },
    ],
    order: [[ 0, "desc" ]]
    });



  });
  /*end datatables*/




    $('#table-data tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            oTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            $this = $(this);
            id = $(this)[0].cells[0].innerHTML;
            console.log(id);
        }
    } );







/*Actualizando Data*/
  $("input#emailData").change(function(){
    trellouser.email = $("input#emailData").val();
    console.log(trellouser.email);
    console.log($("input#emailData").val());
  });

$("input#username").change(function(){
  trellouser.userName = $("input#username").val();
  console.log(trellouser);
});
$("input#userid").change(function(){
  trellouser.id_user = $("input#userid").val();
  console.log(trellouser);
});
$("select[name=departamentos]").change(function() {
  $("#tipoData option:selected").attr("selected", "selected");
  trellouser.dptoUser = $("#departamentos option:selected").val();
  console.log(trellouser);
});


/*fin actualizacion de data*/



/*CRUD METHODS*/

  //=================Crear Usuario ===========================
    $('button#search_user').on('click', function(){
      console.log($('#spinner'));
      $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
      $('#spinner').addClass('loader');
        var email = $('#emailData').val();
        console.log(email);
        console.log('data: '+trellouser.email);
        console.log('email: '+$('#emailData').val());
      fetch(`/trellousers/get/datostrello/${email}`, {
          method: 'GET'
        })
        .then(response => {
          if (response.ok) {

            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .then(data => {
          console.log(data);
          $('#spinner').html("");
          $('#spinner').removeClass('loader');
            $('#i1').css('display','block');
            $('#i2').css('display','block');
            $('#i3').css('display','block');
            $('input#username').val(data.username);
            $('input#userid').val(data.id);
            trellouser.userName = data.username;
            trellouser.id_user = data.id;
            $('#error_dato').empty();
            $('button#btn-create-user').removeAttr('disabled');



        })
        .catch(err => {
          console.log(err);
                if (err) {
                  $('#error_dato').text('El usuario no se encuentra en Trello');
                  $('#spinner').html("");
                  $('#spinner').removeClass('loader');

                  swal({
                    title: "Ups!",
                    text: "Ocurrio un problema, el usuario de trello no se encontró",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                }

        })

    });


  $('button#btn-create-user').on('click', function() {
    console.log(trellouser);
    console.log(trellouser.dptoUser);
    var comodin = false;
    for (let i in trellouser) {
      console.log(trellouser[i]);
      if (trellouser[i] == '' || trellouser[i] == null || trellouser[i] == undefined) {
            console.log(`${trellouser[i]} vacio`);
            swal({
              title: "Verifica",
              text: "Todos los campos deben estar llenos",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          }else{
            comodin = true;
          }

      }

if(comodin === true){
  fetch('/trellousers/create/trellouser', {
      method: "POST",
      body: JSON.stringify(trellouser),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(response => {
      if (response.ok) {
        console.log(response.status);
        console.log(response);
        $('.modal').hide();
        swal("Bien hecho!", "Creaste un nuevo usuario de trello", "success");
        location.reload();

      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    })
    .catch(err => {
      if (err) {
        swal({
          title: "Ups!",
          text: "Ocurrio un problema, el usuario de trello no se creo",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
      }
    })

}
  });
  //===========fin de create =====================


    //==================Editar Usuario =======================================

    $('button.update-user').on('click', function() {
      $('input#emailData').attr('disabled','disabled');
      console.log(id);
      fetch(`/trellousers/get/trellouser/${id}`, {
          method: 'GET'
        })
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .then(data => {
          console.log(data);
          $('#i1').css('display','block');
          $('#i2').css('display','block');
          $('#i3').css('display','block');
          $('input#username').attr('disabled','disabled');

            $('input#username').val(data.userName);
            $('input#emailData').val(data.email);
            $('input#userid').val(data.id_user);
            $(`option#${data.dptoUser._id}`).attr('selected', 'selected');


        })
        .catch(err => {
          // Do something for an error here
        })

    });
    $('button#btn-edit-user').on('click', function() {
      console.log('trellouser');
    console.log(trellouser);
      fetch(`/trellousers/update/trellouser/${id}`, {
        method: "PUT",
        body: JSON.stringify(trellouser),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
      }).then(response => {
        if (response.ok) {
          $('.modal').hide();
          swal("Bien Hecho", "El usuario ha sido editado", "success");
          location.reload();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })

    });


//===========fin editar====================



});
