$(document).ready(function(){

  fetch(`/questions/get/dptos`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }

    })
    .then(data => {
      function compareNumbers(a, b) {
          return a - b;
        }
      var dptos =  data.sort();
      dptos.forEach(function(element) {

        switch(element) {
          case 0:
            var dpto = 'Programacion';
            var li_dpto = '<li role="tab" class="done" aria-disabled="false" aria-selected="false">'+
              '<a class="btn btn-primary select_dpto" data-dpto="2"  data-toggle="collapse" data-target="#demo-1">'+
                dpto+'</a>'+
              '</li>';
              $('ul#dptos').prepend(li_dpto);
            break;
          case 1:
            var dpto = 'Optimizacion y contenido';
            var li_dpto = '<li role="tab" class="done" aria-disabled="false" aria-selected="false">'+
              '<a id= "opti" data-dpto="1"'+
              'class="btn btn-primary select_dpto" data-toggle="collapse" data-target="#demo" >'+
                 dpto+'</a>'+
              '</li>';
              $('ul#dptos').prepend(li_dpto);
            break;
          case 2:
            var dpto = 'Programacion';
            var li_dpto = '<li role="tab" class="done" aria-disabled="false" aria-selected="false">'+
              '<a class="btn btn-primary select_dpto" data-dpto="2"  data-toggle="collapse" data-target="#demo-1">'+
                dpto+'</a>'+
              '</li>';
              $('ul#dptos').prepend(li_dpto);
            break;
          case 3:
            var dpto = 'Marketing';
            var li_dpto = '<li role="tab" class="done" aria-disabled="false" aria-selected="false">'+
              '<a  class="btn btn-primary select_dpto" data-dpto="3"  data-toggle="collapse" data-target="#demo-2">'+
                dpto+'</a>'+
              '</li>';
              $('ul#dptos').prepend(li_dpto);
            break;
          default:
            console.log('tienes un dpto que no coincide');
        }

      });

    })
    .catch(err => {
      // Do something for an error here
    });

//=========================================================

//=========================================================
function brandsDisponibles(dpto){

  $('div.brands_available').empty();
  fetch(`http://localhost:3000/asins/get/asins/${dpto}`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {
        return response.json();

      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }

    })
    .then(data => {
          console.log(data);
          sleep(2000); //Solo se usa para ver el spinner más tiempo en el ejemplo
            $('#spinner').html("");
            $('#spinner').removeClass('loader');
          
                //=====to do ======
                var marcas = [];
                if(data.length > 0){
                  $('#audit').removeAttr('disabled');
                  for(var i=0; i<data.length; i++ ){
                      marcas.push(data[i].brand);

                  }
                }
                Array.prototype.unique=function(a){
                  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
                });

                var result = marcas.unique();
                if(result.length > 0 ){
                  for(var i=0; i<result.length; i++ ){
                      console.log(result);
                      var count = 0;
                      console.log(count);
                      for(var a=0; a<data.length; a++ ){

                          if(data[a].brand === result[i]){
                            count ++;
                          }
                      }
                      console.log(count);
                      console.log(result[i]);
                      //=============Render button =================
                      var button_brand =
                      '<button type="button" class="list-group-item list-group-item-action"> '+
                        ' <span class="badge badge-primary badge-pill label label-success"> '+count+' productos </span>'+
                        result[i]+
                      '</button>';
                        $('div.brands_available').prepend(button_brand);

                      //============================================
                  }
                }else{
                    var msj = '<p>No hay productos de ninguna marca disponibles para realizarle auditoria.</p>';
                    $('div.brands_available').prepend(msj);
                    $('#audit').attr('disabled', 'disabled');
                }



                //==================





          })
          .catch(err => {
            // Do something for an error here
          })


}

//==========================================================


    $("#demo").on("hide.bs.collapse", function(){
          console.log('hide opti');
          $('#demo-1').empty();
          $('#demo-2').empty();
          $('#demo').empty();
          $('.brands_available').empty();
          $('#audit').attr('disabled', 'disabled');
  });
  $("#demo").on("show.bs.collapse", function(event){

    $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
    $('#spinner').addClass('loader');
    $('#demo-1').empty();
    $('#demo-2').empty();
    $('#demo').empty();
    $('.brands_available').empty();
    //========= obtener variables por departamento =======
      var dpto = 1;
      brandsDisponibles(dpto);
    fetch(`/questions/get/questions/${dpto}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {
          return response.json();
          console.log('buscar las marcas');

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
            console.log(data);
            var variables = [];
            if(data.length > 0){
              for(var i=0; i<data.length; i++ ){
                  variables.push(data[i].typeQuestion);

              }
            }
            console.log(variables);
            Array.prototype.unique=function(a){
              return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
            });

            var result = variables.unique();
            console.log(result);
            var button_variables = '<div class="text-center m-t-md">'+
                '<h2>Variables a evaluar en Optimizacion</h2>'+
                '<p>'+
                'Una vez iniciada la auditoria debes completar la evaluacion de todas las variables indicadas.'+
                '</p>'+
                '<div id="var-dpto-2">';
            if(result.length > 0 ){
              for(var i=0; i<result.length; i++ ){
                  console.log(i);
                  var tipoQ = data.filter(tit => tit.typeQuestion == `${result[i]}` );
                  var count = tipoQ.length;
                  //=============Render button =================
                  button_variables += `<button type="button" class="btn btn-primary var">`+
                  `${result[i]}<span class="badge badge-light">${count}</span></button>`;

                    if(i == result.length-1){
                      button_variables += '<p>*El nro indica la cantidad de items a evaluar por variable</p></div>'+
                    '</div>';
                    }

                  //============================================
              }
              console.log(button_variables);
              $('div#demo').prepend(button_variables);
            }

              /*
            var titulo = data.filter(tit => tit.typeQuestion == 'titulo' );
            var desc = data.filter(d => d.typeQuestion == 'descripcion' );
            var totalT = titulo.length;
            var totalD = desc.length;
            var variables =
            '<div class="text-center m-t-md">'+
                '<h2>Variables a evaluar en Optimizacion</h2>'+
                '<p>'+
                'Una vez iniciada la auditoria debes completar la evaluacion de todas las variables indicadas.'+
                '</p>'+
                '<div id="var-dpto-2">'+
                '<button type="button" class="btn btn-primary var">'+
                'Titulo <span class="badge badge-light">'+totalT+'</span></button>'+
                '<button type="button" class="btn btn-primary var">'+
                'Desc y Cont <span class="badge badge-light">'+totalD+'</span></button>'+
                '<p>*El nro indica la cantidad de items a evaluar por variable</p></div>'+
              '</div>';
              $('div#demo').prepend(variables);
*/
              var btn_audit = document.getElementById("audit");
              btn_audit.dataset.dpto = dpto;
            })
            .catch(err => {
              // Do something for an error here
            })

                //======================================================
      });



//=========================
  $("#demo-1").on("hide.bs.collapse", function(){
    console.log('hide opti');
    $('#demo-1').empty();
    $('#demo-2').empty();
    $('#demo').empty();
    $('.brands_available').empty();
    $('#audit').attr('disabled', 'disabled');
  });
  $("#demo-1").on("show.bs.collapse", function(event){
    $('#demo-1').empty();
    $('#demo-2').empty();
    $('#demo').empty();
    $('.brands_available').empty();
    //========= obtener variables por departamento =======
      var dpto = 2;
      brandsDisponibles(dpto);
      console.log('dpto: '+dpto);
    fetch(`/questions/get/questions/${dpto}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
            console.log(data);
            var p1 = data.filter(tit => tit.typeQuestion == 'backend' );
            var p2 = data.filter(d => d.typeQuestion == 'front' );
            var totalT = p1.length;
            var totalD = p1.length;
            console.log(p1);
            console.log(totalT);
            var variables =
            '<div class="text-center m-t-md">'+
                '<h2>Variables a evaluar en Programacion</h2>'+
                '<p>'+
                'Una vez iniciada la auditoria debes completar la evaluacion de todas las variables indicadas.'+
                '</p>'+
                '<div id="var-dpto-1">'+
                '<button type="button" class="btn btn-primary var">'+
                'Titulo <span class="badge badge-light">'+totalT+'</span></button>'+
                '<button type="button" class="btn btn-primary var">'+
                'Desc y Cont <span class="badge badge-light">'+totalD+'</span></button>'+
                '<p>*El nro indica la cantidad de items a evaluar por variable</p></div>'+
              '</div>';
            $('div#demo-1').prepend(variables);

            var btn_audit = document.getElementById("audit");
            btn_audit.dataset.dpto = dpto;
            })
            .catch(err => {
              // Do something for an error here
            })

                //======================================================
  });
  //====================================
  $("#demo-2").on("hide.bs.collapse", function(){
    console.log('hide opti');
    $('#demo-1').empty();
    $('#demo-2').empty();
    $('#demo').empty();
    $('.brands_available').empty();
    $('#audit').attr('disabled', 'disabled');
});
$("#demo-2").on("show.bs.collapse", function(event){
  $('#demo-1').empty();
  $('#demo-2').empty();
  $('#demo').empty();
  $('.brands_available').empty();
  //========= obtener variables por departamento =======
    var dpto = 3;
      brandsDisponibles(dpto);
  fetch(`/questions/get/questions/${dpto}`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }

    })
    .then(data => {
          console.log(data);
          var p1 = data.filter(tit => tit.typeQuestion == 1 );
          var p2 = data.filter(d => d.typeQuestion == 2 );
          var totalT = p1.length;
          var totalD = p2.length;
          var variables =
          '<div class="text-center m-t-md">'+
              '<h2>Variables a evaluar en Marketing</h2>'+
              '<p>'+
              'Una vez iniciada la auditoria debes completar la evaluacion de todas las variables indicadas.'+
              '</p>'+
              '<div id="var-dpto-2">'+
              '<button type="button" class="btn btn-primary var">'+
              'Titulo <span class="badge badge-light">'+totalT+'</span></button>'+
              '<button type="button" class="btn btn-primary var">'+
              'Desc y Cont <span class="badge badge-light">'+totalD+'</span></button>'+
              '<p>*El nro indica la cantidad de items a evaluar por variable</p></div>'+
            '</div>';
          $('div#demo-2').prepend(variables);

          var btn_audit = document.getElementById("audit");
          btn_audit.dataset.dpto = dpto;
          })
          .catch(err => {
            // Do something for an error here
          })

              //======================================================

});
//=====================================================

$('#audit').on('click',  function(){
    console.log('press button');

    var dpto = parseInt(this.dataset.dpto);
    console.log(dpto);
    if(dpto){
      swal("Hey!", "No se ha seleccionado un departamento", "error")
    }
    var dptoName = "";
    console.log(dpto);
    switch(dpto) {
      case 1:
            dptoName = "Optimizacion y contenido"
        break;
      case 2:
        dptoName = "Programacion"
        break;
      case 3:
        dptoName = "Marketing"
        break;
      default:
          dptoName = "No seleccionado"
        };
    console.log(`Empezaras una auditoria de ${dptoName}`);
    swal({
        title: "Estas seguro?",
        text: `Empezaras una auditoria de ${dptoName}`,
        type: "warning",
        buttons: true,
        dangerMode: true,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, auditar!",
        closeOnConfirm: false
      },
      function(){
          location.href=`http://localhost:3000/audits/products/amazon/${dpto}`;
      })
    });



});


  function sleep(milisegundos) {
    console.log('ejecutando sleep');
    var comienzo = new Date().getTime();
    while (true) {
      if ((new Date().getTime() - comienzo) > milisegundos)
        break;
    }
  }
