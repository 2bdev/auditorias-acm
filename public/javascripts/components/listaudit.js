$(document).ready(function() {
  var user = $('#userlog').attr('data-user');
  var tipo = $('#userlog').attr('data-tipo');
  if(tipo == 'superadmin' || tipo == 'admin'){
    fetch(`/audits/get/audits`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log(data);
        if(data.length > 0 ){
          $('#table-data').DataTable( {
              data: data,
              language: {
                "emptyTable":     "No hay datos disponibles",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "search":         "Buscar:",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
              },
              columns: [
                { data: 'audit_nro', "defaultContent":"-"},
                { data: 'tipo.name', "defaultContent":"-"},
                { data: 'fecha_audit', "defaultContent":"-" },
                { data: 'link.asinParent', "defaultContent":"-" },
                { data: 'link.brand', "defaultContent":"-" },
                { data: 'usuario.email', "defaultContent":"-" },
                { data: 'result', "defaultContent":"-" },
                { data: 'evaluacion', "defaultContent":"-",
                "render": function(data, type, row, meta){

                  var content = ''
                  data.forEach(function(el) {
                        content = content + ` ${el.variable}: ${Math.trunc(el.porcentaje)}% `;
                  });
                  var popover = `<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="${content}">
    <i class="fas fa-list-alt"></i>
  </button>`
                  return popover;
                }
              },
            ],

            order: [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                {
                extend: 'collection',
                text: 'Exportar',
                buttons: [
                    'excel',
                    'pdf']
                }
            ]
            });
        }else{
          $('.table-responsive').append('<h3>No hay datos disponibles</h3>');

        }

        })
        .catch(err => {

        })
  }
  if(tipo == 'acm'){
    fetch(`/audits/get/misaudits/${user}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log(data);
        if(data.length > 0 ){
          $('#table-data').DataTable( {
              data: data,
              language: {
                "emptyTable":     "No hay datos disponibles",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "search":         "Buscar:",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
              },
              columns: [
                { data: 'audit_nro', "defaultContent":"-"},
                { data: 'tipo.name', "defaultContent":"-"},
                { data: 'fecha_audit', "defaultContent":"-" },
                { data: 'link.asinParent', "defaultContent":"-" },
                { data: 'link.brand', "defaultContent":"-" },
                { data: 'usuario.email', "defaultContent":"-" },
                { data: 'result', "defaultContent":"-" },
                { data: 'evaluacion', "defaultContent":"-",
                "render": function(data, type, row, meta){

                  var content = ''
                  data.forEach(function(el) {
                        content = content + ` ${el.variable}: ${Math.trunc(el.porcentaje)}% `;
                  });
                  var popover = `<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="${content}">
    <i class="fas fa-list-alt"></i>
  </button>`
                  return popover;
                }
              },
            ],

            order: [[ 0, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                {
                extend: 'collection',
                text: 'Exportar',
                buttons: [
                    'excel',
                    'pdf']
                }
            ]
            });
        }else{
          $('.table-responsive').append('<h3>No hay datos disponibles</h3>');

        }

        })
        .catch(err => {

        })
  }


});
