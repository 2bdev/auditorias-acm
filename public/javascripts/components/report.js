$(document).ready(function() {
  var user = $('#userlog').attr('data-user');
  var tipo = $('#userlog').attr('data-tipo');
  if(tipo == 'admin' || tipo == 'superadmin'){
    fetch(`/audits/report/audits`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log(data);
        if(data.length > 0 ){
          $('#table-data').DataTable( {
              data: data,
              language: {
                "emptyTable":     "No hay datos disponibles",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "search":         "Buscar:",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
              },
              columns: [
                { data: 'fecha', "defaultContent":"-" },
                { data: 'tipo', "defaultContent":"-"},
                { data: 'asin', "defaultContent":"-" },
                { data: 'sku', "defaultContent":"-" },
                { data: 'brand', "defaultContent":"-" },
                { data: 'user', "defaultContent":"-" },
                { data: 'result', "defaultContent":"-",
                "render": function(data, type, row, meta){

                     var b= Math.round(data * 100) / 100;
                     var resultado = `${b}%`
                  return resultado;
                }

               },
                { data: 'variable', "defaultContent":"-" },
                { data: 'porcent', "defaultContent":"-",
                "render": function(data, type, row, meta){

                     var b= Math.round(data * 100) / 100;
                     var resultado = `${b}%`
                  return resultado;
                }
               },
                { data: 'p', "defaultContent":"-" },
                { data: 'r',
                "defaultContent":"-",
                "render": function(data, type, row, meta){
                  if(type === 'display'){
                    if (data == true) {
                      data = '<span class="badge badge-primary badge-pill label label-success">true</span>';
                    }else{
                      data = '<span class="badge badge-primary badge-pill label label-danger"> false </span>';
                    }
                  }
                  return data;
                }

              }

            ],

            dom: 'Bfrtip',
            buttons: [
                {
                extend: 'collection',
                text: 'Exportar',
                buttons: [
                    'excel'
                    ]
                }
            ]
            });
        }else{
          $('.table-responsive').append('<h3>No hay datos disponibles</h3>');

        }

        })
        .catch(err => {
            console.log(`err: ${err}`);
        })
  }else{
      //report perzonalizado
      fetch(`/audits/report/user/${id}`, {
          method: 'GET'
        })
        .then(response => {
          if (response.ok) {

            return response.json();

          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .then(data => {
          console.log(data);
          if(data.length > 0 ){
            $('#table-data').DataTable( {
                data: data,
                language: {
                  "emptyTable":     "No hay datos disponibles",
                  "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                  "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                  "search":         "Buscar:",
                  "lengthMenu":     "Mostrar _MENU_ entradas",
                  "paginate": {
                      "first":      "Primero",
                      "last":       "Ultimo",
                      "next":       "Siguiente",
                      "previous":   "Anterior"
                  }
                },
                columns: [
                  { data: 'fecha', "defaultContent":"-" },
                  { data: 'tipo', "defaultContent":"-"},
                  { data: 'asin', "defaultContent":"-" },
                  { data: 'sku', "defaultContent":"-" },
                  { data: 'brand', "defaultContent":"-" },
                  { data: 'user', "defaultContent":"-" },
                  { data: 'result', "defaultContent":"-",
                  "render": function(data, type, row, meta){

    	                 var b= Math.round(data * 100) / 100;
                       var resultado = `${b}%`
                    return resultado;
                  }

                 },
                  { data: 'variable', "defaultContent":"-" },
                  { data: 'porcent', "defaultContent":"-",
                  "render": function(data, type, row, meta){

                       var b= Math.round(data * 100) / 100;
                       var resultado = `${b}%`
                    return resultado;
                  }
                 },
                  { data: 'p', "defaultContent":"-" },
                  { data: 'r',
                  "defaultContent":"-",
                  "render": function(data, type, row, meta){
                    if(type === 'display'){
                      if (data == true) {
                        data = '<span class="badge badge-primary badge-pill label label-success">true</span>';
                      }else{
                        data = '<span class="badge badge-primary badge-pill label label-danger"> false </span>';
                      }
                    }
                    return data;
                  }

                }

              ],

              dom: 'Bfrtip',
              buttons: [
                  {
                  extend: 'collection',
                  text: 'Exportar',
                  buttons: [
                      'excel'
                      ]
                  }
              ]
              });
          }else{
            $('.table-responsive').append('<h3>No hay datos disponibles</h3>');

          }

          })
          .catch(err => {
              console.log(`err: ${err}`);
          })


  }


});
