$(document).ready(function() {
  var user = $('#userlog').attr('data-user');
  var tipo = $('#userlog').attr('data-tipo');
  if(tipo == 'superadmin' || tipo == 'admin'){
    fetch(`/trello/get/cards`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

    })
    .then(data => {
      console.log(data);
      if(data.length > 0 ){
        $('#table-data').DataTable( {
            data: data,
            language: {
              "emptyTable":     "No hay datos disponibles",
              "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
              "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
              "search":         "Buscar:",
              "lengthMenu":     "Mostrar _MENU_ entradas",
              "paginate": {
                  "first":      "Primero",
                  "last":       "Ultimo",
                  "next":       "Siguiente",
                  "previous":   "Anterior"
              }
            },
            columns: [
              { data: 'name', "defaultContent":"-"},
              { data: 'linkBoard', "defaultContent":"-",
              "render": function(data){
                var enlace = `<a href="${data}">${data}</a>`;
                return enlace;
              }
            },
              { data: 'userAutor.trellouser.userName', "defaultContent":"-" },
              { data: 'audit.link.asinParent', "defaultContent":"-" },
              { data: 'audit.link.sku', "defaultContent":"-" }
            ],
            dom: 'Bfrtip',
            buttons: [
                {
                extend: 'collection',
                text: 'Exportar',
                buttons: [
                    'excel'
                  ]
                }]
              });
        }else{
          $('.table-responsive').append('<h3>No hay datos disponibles</h3>');
        }

        })
        .catch(err => {
            console.log(`Err: ${err}`);
        })

  }
  if(tipo == 'acm'){
    fetch(`/trello/get/miscards/${user}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();

        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log(data);
        if(data.length > 0 ){
          $('#table-data').DataTable( {
              data: data,
              language: {
                "emptyTable":     "No hay datos disponibles",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
                "search":         "Buscar:",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
              },
              columns: [
                { data: 'name', "defaultContent":"-"},
                { data: 'linkBoard', "defaultContent":"-",
                "render": function(data){
                  var enlace = `<a href="${data}">${data}</a>`;
                  return enlace;
                }
              },
                { data: 'userAutor.trellouser.userName', "defaultContent":"-" },
                { data: 'audit.link.asinParent', "defaultContent":"-" },
                { data: 'audit.link.sku', "defaultContent":"-" }
              ],
              dom: 'Bfrtip',
              buttons: [
                  {
                  extend: 'collection',
                  text: 'Exportar',
                  buttons: [
                      'excel',
                      'pdf']
                  }
              ]
            });
        }else{
          $('.table-responsive').append('<h3>No hay datos disponibles</h3>');
        }

        })
        .catch(err => {
            console.log(`Err: ${err}`);
        })

  }

});
