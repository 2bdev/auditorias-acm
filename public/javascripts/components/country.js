$('#exampleModal').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  console.log(button);
  var recipient = button[0].dataset.title; // Extract info from data-* attributes
  console.log(recipient);
  if(recipient == 'Crear Pais'){
    $('#btn-save-q').show();
    $('#btn-edit-q').hide();
  }else{
    $('#btn-save-q').hide();
    $('#btn-edit-q').show();
  }
  var modal = $(this)
  modal.find('.modal-title').text(recipient)
  modal.show();
});

//==================Editar Usuario =======================================
var datos = {
  name: String,
  url: String
};

$(document).ready(function(){
  /*dataTables begin*/
  fetch(`/countries/get/countries`, { method: 'GET' }) .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    var oTable = $('#table').DataTable({
      data: data,
      columns: [
        { data: 'name', "defaultContent":"-" },
        { data: 'url', "defaultContent":"-" },
        // {
        //   "targets": -1,
        //   "data": null,
        //   "defaultContent": "<button class='btn btn-primary update-q'><i class='far fa-edit'></i><span>Editar</span></button>"
        // },
        // {
        //   "targets": -1,
        //   "data": null,
        //   "defaultContent": "<button class='btn btn-primary delete-q'><i class='fas fa-trash-alt'></i><span>Eliminar</span></button>"
        // }
      ]
    });
  });
  /*end datatables*/
  $("input#name").change(function(){
    datos.name = $("input#name").val();
    console.log(datos);
  });
  $("input#link").change(function(){
    datos.url = $("input#link").val();
    console.log(datos);
  });
});

//=================Crear Pregunta =====================================
$('button#btn-save-q').on('click', function(){
  fetch(`/countries/create/country`, {
    method: "POST",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  }).then(response => {
    if (response.ok) {
      console.log(response.status);
      console.log(response);
      $('.modal').hide();
      swal("Bien Hecho!", "Creaste una nueva pregunta", "success");
      location.reload();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  })

});




//=================Eliminar Pregunta =====================================

$('td#delete-q').on('click', function (){
  var id = $(this).attr('data-q');
  console.log(id);
  console.log('td#delete-q');
  var url = '/questions/delete/question/'+id;
  swal({
    title: "Estas seguro?",
    text: "Una vez eliminado, esta pregunta se desactivará!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  },
  function(isConfirm){
    if (isConfirm) {
      return fetch(url, {
        method: 'delete'
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          console.log(response);
          location.reload();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })
      swal("Poof! Pregunta eliminada", {
        icon: "success",
      });
    } else {
      swal("Cancelled", "Tu pregunta esta a salvo :)", "error");
    }
  });
})


$('td#activar-q').on('click', function (){
  var id = $(this).attr('data-q');
  console.log(id);
  console.log('td#activar-q');
  var url = '/questions/activate/question/'+id;
  swal({
    title: "Estas seguro?",
    text: "Una vez activada, esta pregunta estará disponible!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  },
  function(isConfirm){
    if (isConfirm) {
      return fetch(url, {
        method: 'PUT'
      })
      .then(response => {
        if (response.ok) {
          console.log(response.status);
          console.log(response);
          location.reload();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })
      swal("Poof! Pregunta activada", {
        icon: "success",
      });
    } else {
      swal("Cancelled", "Tu pregunta esta a salvo :)", "error");
    }
  });
})
