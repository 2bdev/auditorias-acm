/* GENERAL */


//================== Functions ================================
$(document).ready(function(){
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');

//========= obtener departamentos
fetch(`/trellousers/get/datostrello/web.development@2becommerce.com`, {
    method: 'GET'
  })
  .then(response => {
    if (response.ok) {

      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }

  })
  .then(data => {
    console.log(data);
    fetch(`departamentos/get/boardtrello/${data.id}`, {
        method: 'GET'
      })
      .then(response => {
        if (response.ok) {

          return response.json();
        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }

      })
      .then(data => {
        console.log(data);
        data.forEach(function(element) {
          var id = element.id
          var name = element.name;
          var shortUrl = element.shortUrl;
          var option = '<option data-name="'+name+'" data-url="'+shortUrl+'" id="'+name+'" value="' + id + '">' + name + '</option>';
          $('#tableros').prepend(option);
        });
        $('#spinner').html("");
        $('#spinner').removeClass('loader');
        $('#i0').css('display','block');
        $("select[name=tableros]").change(function() {
          $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
          $('#spinner').addClass('loader');

          $("#tableros option:selected").attr("selected", "selected");
          var board = $("#tableros option:selected").val();
          var name = $("#tableros option:selected").attr('data-name');
          var shortUrl = $("#tableros option:selected").attr('data-url');
          datos.idBoard = board;
          datos.linkBoard = shortUrl;
          datos.nameBoard = name;
          $('input#nameBoard').val(name);
          $('input#linkBoard').val(shortUrl);
          console.log(board);
          console.log('idBoard');
          console.log(datos.idBoard);
            //==========  Mostrar datos del lista del tablero  =======================
            fetch(`departamentos/get/listtrello/${board}`, {
                method: 'GET'
              })
              .then(response => {
                if (response.ok) {

                  return response.json();
                } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }

              })
              .then(data => {
                console.log(data);
                if(data.idBoard){
                  $('#spinner').html("");
                  $('#spinner').removeClass('loader');
                  $('#i1').css('display','block');
                  $('#i2').css('display','block');
                  $('#i3').css('display','block');
                  $('#i4').css('display','block');
                  $('#i5').css('display','block');
                  $('#i6').css('display','block');
                  $('#esp').css('display','block');
                  //$('input#name').val(data.nameBoard);
                  $('input#nameList').val(data.listName);
                  //  $('input#linkBoard').val(data.shortUrl);
                    $('input#idList').val(data.idList);
                    //datos.linkBoard = $("input#linkBoard").val();
                    datos.idList =$('input#idList').val();
                    datos.nameList = data.listName;
                    $('#error_dato').empty();
                    $('button#btn-save-q').removeAttr('disabled');

                }else{
                  $('#spinner').html("");
                  $('#spinner').removeClass('loader');
                  $('#error_dato').text('Ocurrio un problema, intenta con otro tablero');
                }

              })
              .catch( err =>{
                swal("Este Tablero no tiene una lista creada", "comunicate con el Coordinador e intenta nuevamente");

              } )
        });

      })

  })
  .catch(err => {
    console.log(err);
          if (err) {
            $('#error_dato').text('El usuario no se encuentra en Trello');
            $('#spinner').html("");
            $('#spinner').removeClass('loader');

            swal({
              title: "Ups!",
              text: "Ocurrio un problema, el usuario de trello no se encontró",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          }

  })




//==========
  var oTable = '';
  var datos = {};

  //=========== obtener datos del usuario al abrir el modal ====
  let activarDesactivar = (opcion, id) =>{
    console.log(id);
    console.log(opcion);
    if(opcion == 'delete'){
      swal({
          title: "Estas seguro?",
          text: "Una vez desactivado, este usuario no estará disponible",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Si , Desactivar!",
          closeOnConfirm: false
        },
        function(isConfirm){
          if (isConfirm) {
            return fetch(`/departamentos/desactivar/departamento/${id}`, {
                method: 'delete'
              })
              .then(response => {
                if (response.ok) {
                  location.reload();
                } else {
                  return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
                  })
                }
              })
            swal("Poof! Usuario desactivado", {
              icon: "success",
            });
          } else {
            swal("El usuario no se ha desactivado!");
          }
        });
    }
    if(opcion == 'activate'){
        console.log('activadonos');
        swal({
            title: "Estas seguro?",
            text: "Una vez activado, este usuario estará disponible",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si , Activar!",
            closeOnConfirm: false
          },
          function(isConfirm){
            if (isConfirm) {
              return fetch(`/departamentos/activate/departamento/${id}`, {
                  method: 'delete'
                })
                .then(response => {
                  if (response.ok) {
                    location.reload();
              } else {
                    return Promise.reject({
                      status: response.status,
                      statusText: response.statusText
                    })
                  }
                })
              swal("Poof! Departamento activado", {
                icon: "success",
              });
            } else {
              swal("El departamento ha sido activado!");
            }
          });
    }
  }


  let obtenerDatosDpto = (idp) => {
    console.log(idp);
    if(idp != '' || idp != undefined){
      fetch(`/departamentos/get/departamento/${idp}`, {
          method: 'GET'
        })
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .then(data => {
          console.log(data);
          if(data.status == 1){
            $('#spinner').html("");
            $('#spinner').removeClass('loader');

            $('input#name').val(data.name);
            $('input#linkBoard').val(data.linkBoard);
            $('input#nameBoard').val(data.nameBoard);
            $('input#nameList').val(data.nameList);
            $('input#variables').val(data.variables);
            $('input#idList').val(data.idList);


        }else{
          $('#spinner').html("");
          $('#spinner').removeClass('loader');

            swal("Ups!", "Este departamento esta desactivado y no puede ser editado", "warning");
            location.reload();
          }

        })
        .catch(err => {
          console.log(`err ${err}`);
          // Do something for an error here
        })
    }else {
      $('#spinner').html("");
      $('#spinner').removeClass('loader');

      swal("Ups!", "Debes seleccionar un departamento", "warning");
    }


  }


  // ========== fin de obtener datos



  /* MODAL */
  $('#exampleModal').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget) // Button that triggered the modal
    console.log(button);
    var recipient = button[0].dataset.title; // Extract info from data-* attributes
    console.log(recipient);
    if(recipient == 'Crear Departamento'){
      $('#btn-save-q').show();
      $('#btn-edit-q').hide();
      $('#i0').css('display','block');
      $('#i1').css('display','none');
      $('#i2').css('display','none');
      $('#i3').css('display','none');
      $('#i4').css('display','none');
      $('#i5').css('display','none');
      $('#i6').css('display','none');
      $('#esp').css('display','none');

      $('#mailS').css('display', 'none');
      $('input#name').val('')
      $('input#variables').val('')
      $('input#linkBoard').attr('disabled','disabled').val('');
      $('input#nameBoard').attr('disabled','disabled').val('');
      $('input#nameList').attr('disabled','disabled').val('');
      $('input#idList').attr('disabled','disabled').val('');

    }else{
      $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
      $('#spinner').addClass('loader');

      $('#btn-save-q').hide();
      $('#btn-edit-q').show();
      $('#i0').css('display','none');
      $('#esp').css('display','block');
      $('#i1').css('display','block');
      $('#i2').css('display','block');
      $('#i3').css('display','block');
      $('#i4').css('display','block');
      $('#i5').css('display','block');
      $('#i6').css('display','block');
      $('#esp').css('display','block');

      $('#mailS').css('display', 'none');
      $('input#linkBoard').attr('disabled','disabled');
      $('input#nameBoard').attr('disabled','disabled');
      $('input#nameList').attr('disabled','disabled');
      $('input#idList').attr('disabled','disabled');

      var idp = button[0].dataset.id;
      id = button[0].dataset.id;
      obtenerDatosDpto(idp);
    }
    var modal = $(this)
    modal.find('.modal-title').text(recipient)
    modal.show();
  });



  /*dataTables begin*/
  fetch(`/departamentos/get/departamentos`, { method: 'GET' }).then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  }).then(data => {
    var datos = data;
    var usertipo =  $('#user').attr('data-type');
    if(usertipo == 'admin'){
      datos = data.filter(dpto => dpto.tipo == 'a');
    }
    oTable = $('#table').DataTable({
      data: datos,
      language: {
        "emptyTable":     "No hay datos disponibles",
        "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
        "search":         "Buscar:",
        "lengthMenu":     "Mostrar _MENU_ entradas",
        "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        }
      },
      columns: [
        { data: 'name', "defaultContent":"-" },
        { data: 'variables', "defaultContent":"-" },
        { data: 'linkBoard', "defaultContent":"-" },
        { data: 'idList', "defaultContent":"-" },
        { data: 'status',
        "defaultContent":"-",
        "render": function(data, type, row, meta){
          if(type === 'display'){
            if (data == '1') {
              data = '<span class="badge badge-primary badge-pill label label-success">Active</span>';
            }else{
              $(`button.edit[data-id = ${row._id}]`).attr('disabled', 'disabled');
              $(`button.delete[data-id = ${row._id}]`).addClass('activate');
              $(`button.activate[data-id = ${row._id}]`).removeClass('delete');
              $(`button.activate[data-id = ${row._id}]`).removeClass('btn-primary');
              $(`button.activate[data-id = ${row._id}]`).addClass('btn-danger');
              $(`button.activate[data-id = ${row._id}]`).html('<span aria-hidden="true"><i class="fas fa-trash-restore-alt"></i></span>');
              data = '<span class="badge badge-primary badge-pill label label-danger"> Inactive </span>';
            }
          }
          return data;
        }

      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button  type="button" data-title='editar'  data-toggle='modal' data-target='#exampleModal' class="btn btn-primary option edit" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-edit"></i></span>`+
            `</button>`

        return boton;
      }
      },
      {data: '_id',
      "defaultContent":"-",
      "render": function(data, type, row, meta){
            var boton = `<button type="button" name="delete" class="btn btn-primary option delete" data-id="${data}" >`+
              `<span aria-hidden="true"><i class="far fa-trash-alt"></i></span>`+
            `</button>`

        return boton;
      }
      }
      ]
    });

    //seleccionar una fila
    $('button.option').on( 'click', function () {
      id = $(this).attr('data-id');
    if ($(this).hasClass("delete")){
      var opcion = 'delete';
      activarDesactivar(opcion, id);
    }else if($(this).hasClass("activate")){
      var opcion = 'activate';
      activarDesactivar(opcion, id);
    }
    });

    $('#spinner').html("");
    $('#spinner').removeClass('loader');

  }); //fin dataTables


//Actualizar datos{}
$("input#name").change(function(){
  datos.name = $("input#name").val();
  console.log(datos);
});
$("input#tipo").change(function(){
  datos.tipo = $("input#tipo").val();
  console.log(datos);
});
$("input#linkBoard").change(function(){
  datos.linkBoard = $("input#linkBoard").val();
  console.log(datos);
});


$("input#variables").change(function(){

    if (document.getElementById("variables").value.indexOf(" ") !== -1) {
      swal("Algo va mal!", "recuerda sin espacios en blanco y separadas por coma", "warning");

    }else{
      var arreglo = $("input#variables").val().split(',');
      console.log(arreglo);
      if (arreglo.indexOf("") !== -1) {
        swal("Algo va mal!", "recuerda usar la coma (,) solo para separar las variables", "warning");
      }else{
        datos.variables = arreglo;
        console.log(datos.variables);
      }

    }


});

$('button#search_user').on('click', function(){
  console.log($('#spinner'));
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');
    //var email = $('#emailData').val();
    //console.log(email);
  fetch(`/trellousers/get/datostrello/web.development@2becommerce.com`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {

        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }

    })
    .then(data => {
      console.log(data);
      fetch(`departamentos/get/boardtrello/${data.id}`, {
          method: 'GET'
        })
        .then(response => {
          if (response.ok) {

            return response.json();
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText
            })
          }

        })
        .then(data => {
          console.log(data);
          data.forEach(function(element) {
            var id = element.id
            var name = element.name;
            var shortUrl = element.shortUrl;
            var option = '<option data-name="'+name+'" data-url="'+shortUrl+'" id="'+name+'" value="' + id + '">' + name + '</option>';
            $('#tableros').prepend(option);
          });
          $('#spinner').html("");
          $('#spinner').removeClass('loader');
          $('#i0').css('display','block');
          $("select[name=tableros]").change(function() {
            $("#tableros option:selected").attr("selected", "selected");
            var board = $("#tableros option:selected").val();
            var name = $("#tableros option:selected").attr('data-name');
            var shortUrl = $("#tableros option:selected").attr('data-url');
            datos.idBoard = board;
            datos.linkBoard = shortUrl;
            datos.nameBoard = name;
            $('input#nameBoard').val(name);
            $('input#linkBoard').val(shortUrl);
            console.log(board);
            console.log('idBoard');
            console.log(datos.idBoard);
              //==========  Mostrar datos del lista del tablero  =======================
              fetch(`departamentos/get/listtrello/${board}`, {
                  method: 'GET'
                })
                .then(response => {
                  if (response.ok) {

                    return response.json();
                  } else {
                    return Promise.reject({
                      status: response.status,
                      statusText: response.statusText
                    })
                  }

                })
                .then(data => {
                  console.log(data);
                  if(data.idBoard){
                    $('#spinner').html("");
                    $('#spinner').removeClass('loader');
                    $('#esp').css('display','block');
                    $('#i1').css('display','block');
                    $('#i2').css('display','block');
                    $('#i3').css('display','block');
                    $('#i4').css('display','block');
                    $('#i5').css('display','block');
                    $('#i6').css('display','block');
                    $('#esp').css('display','block');
                    //$('input#name').val(data.nameBoard);
                    $('input#nameList').val(data.listName);
                    //  $('input#linkBoard').val(data.shortUrl);
                      $('input#idList').val(data.idList);
                      //datos.linkBoard = $("input#linkBoard").val();
                      datos.idList =$('input#idList').val();
                      datos.nameList = data.listName;
                      $('#error_dato').empty();
                      $('button#btn-save-q').removeAttr('disabled');

                  }else{
                    $('#error_dato').text('Intenta con Otro Usuario');
                  }

                })
                .catch( err =>{
                  swal("Este Tablero no tiene una lista creada", "comunicate con el Coordinador e intenta nuevamente");

                } )
          });

        })

    })
    .catch(err => {
      console.log(err);
            if (err) {
              $('#error_dato').text('El usuario no se encuentra en Trello');
              $('#spinner').html("");
              $('#spinner').removeClass('loader');

              swal({
                title: "Ups!",
                text: "Ocurrio un problema, el usuario de trello no se encontró",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
            }

    })

}); //fin search user por el dpto


/*create department*/
$('button#btn-save-q').on('click', function(){
  console.log(datos);
  fetch(`/departamentos/create/departamentos`, {
    method: "POST",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  }).then(response => {
    if (response.ok) {
      console.log(response.status);
      console.log(response);
      $('.modal').hide();
      swal("Bien Hecho!", "Creaste una nueva departamento", "success");
      location.reload();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  })
});

/*create department*/
$('button#btn-edit-q').on('click', function(){
  console.log(datos);
  console.log(id);
  fetch(`/departamentos/update/departamento/${id}`, {
    method: "POST",
    body: JSON.stringify(datos),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  }).then(response => {
    if (response.ok) {
      console.log(response.status);
      console.log(response);
      $('.modal').hide();
      swal("Bien Hecho!", "Editaste un departamento", "success");
      location.reload();
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText
      })
    }
  })
});

}); //fin
