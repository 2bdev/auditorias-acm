// TODO: verificar proceso  de desencriptacion
$(document).ready(function(){
  $('#i0').css('display','none');
  $('#i1').css('display','none');
  var id = $('#profile_user').attr('data-id');
//verificar si es la contraseña Actual
$('input#passuser').change(function(){
  var pass = $('input#passuser').val();
  console.log($('#spinner'));
  $('#spinner').html('<center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></center>');
  $('#spinner').addClass('loader');

  fetch(`/users/confirmar/user/${id}/${pass}`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) {

        return response.json();
      } else {
        return Promise.reject({
          status: response.status,
          statusText: response.statusText
        })
      }
    })
    .then(data => {
      console.log(data);
      if(data == true){
        //habilitar cambio de contraseña
        $('#i0').css('display','block');
      }else{
          swal("Ups", "La contraseña es incorrecta", "warning");
      }

    })
    .catch( err =>{
      swal("Ups", `Ocurrio un problema -- ${err}`);

    })


})


var nueva = '';
var conf = '';
//verificar que la contraseña de confirmacion sea igual a la nueva
$('input#newpass').change(function(){
  nueva = $('input#newpass').val();
  $('#i1').css('display','block');

})
$('input#confpass').change(function(){
  conf = $('input#confpass').val();
  if( conf === nueva){
    $('#texterror').text('Contraseña correcta');
      $('button#savePass').removeAttr('disabled');
  }else{
    $('#texterror').text('Contraseña incorrecta');
      $('button#savePass').attr('disabled', 'disabled');
  }

})
//guardar la nueva contraseña
$('button#savePass').on('click', function(){
  var confirmacion = $('input#confpass').val();
  var datos = {
    password: confirmacion
  }
  console.log(datos);
  var id = $('#profile_user').attr('data-id');
  console.log(id);
  fetch(`/users/cambiarpass/user/${id}`, {
      method: "PUT",
      body: JSON.stringify(datos),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
      }).then(response => {
        if (response.ok) {
          console.log(response.status);
          console.log(response);
          swal("Bien Hecho!", "Cambiaste la contraseña!", "success");


        } else {
          return Promise.reject({
            status: response.status,
            statusText: response.statusText
          })
        }
      })
      .catch( err =>{
        swal("Ups", "Ocurrio un problema");

      })

})



  $('button#show_password').on('click',  function (){

  		var cambio = document.getElementById("passuser");
  		if(cambio.type == "password"){
  			cambio.type = "text";
  			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
  		}else{
  			cambio.type = "password";
  			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
  		}

  	});

    $('button#show_pass').on('click',  function (){

        var cambio = document.getElementById("newpass");
        if(cambio.type == "password"){
          cambio.type = "text";
          $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        }else{
          cambio.type = "password";
          $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }

      });

      $('button#show_pass_conf').on('click',  function (){

          var cambio = document.getElementById("confpass");
          if(cambio.type == "password"){
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
          }else{
            cambio.type = "password";
            $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
          }

        });


})
