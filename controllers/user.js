var User = require('../models/user');
var Trellouser = require('../models/trellouser');
var bcrypt = require('bcrypt');
var userController = {};


userController.createUser = function(req, res, next) {
  var password = req.body.password;
  var newUser = new User();
  newUser.email = req.body.email;
  newUser.tipo = req.body.tipo;
  newUser.password = newUser.generateHash(password);
  newUser.trellouser = req.body.trellouser;
  newUser.marcas = req.body.marcas;

  newUser.save(function(err) {
    if (err)
      throw err;
    res.send('El user se añadio de forma exitosa')
  });



};

userController.detailsUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  User.findById(objId)
    .populate('trellouser')
    .exec(function(err, user) {
      if (err) return res.status(500).send({
        message: `Ocurrio un problema`
      });
      if (!user) return res.status(404).send({
        message: `Not found ${user}`
      });

      res.send(user);

    });
};

userController.allUser = (req, res) => {
  User.find()
    .populate('trellouser')
    .exec(function(err, users) {
      if (err) return res.status(500).send({
        message: `Ocurrio un problema`
      });
      if (!users) return res.status(404).send({
        message: `Not found`
      });

      res.send(users);

    });
};

userController.updateUser = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;

  console.log(objId);
  console.log(updated);

User.findOneAndUpdate({_id: objId}, updated, (err, user) => {
    if (err) return res.status(500).send({message: `No se actualizo  ${user}`});
    res.send(user);
  });
};

userController.deleteUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  User.findOneAndUpdate({
    _id: objId
  }, obj, (err, user) => {
    if (err) return res.status(500).send({
      message: `No se elimino  ${user}`
    });
    res.send('Eliminado el usuario');
  });
};


userController.statusUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  User.findOneAndUpdate({
    _id: objId
  }, obj, (err, user) => {
    if (err) return res.status(500).send({
      message: `No se elimino  ${user}`
    });
    res.send(`Eliminado el Usuario ${user}`);
  });
};

userController.activateUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 1 };
  User.findOneAndUpdate({
    _id: objId
  }, obj, (err, user) => {
    if (err) return res.status(500).send({
      message: `No se activo  ${user}`
    });
    res.send('Activado el Usuario');
  });
};

userController.confirmPass = function(req, res, next) {

  let id = req.params.id;
  let pass = req.params.pass;


    User.findById(id, (err, asin) => {
      console.log(asin);
        if(asin.validPassword(pass) == true){
          var msj = true
          res.send(msj);

        }else{
          var msj = false
          res.send(msj);
        }


    })

};

userController.updatePass = (req, res) => {
  let id = req.params.id;
  let updated = req.body;
  console.log(updated);
  updated.password = bcrypt.hashSync(updated.password, bcrypt.genSaltSync(8), null);
User.findOneAndUpdate({_id: id}, updated, function(err,user){
      if (err) return res.status(500).send({
        message: `No Editada  ${err}`
      });
      res.send('EDitada');

  });
  }

  userController.eliminarUsuario = (req, res)=> {
    let objId = req.params.id;

    User.findOneAndDelete({_id: objId}, (err,  user) =>{
      if (err) return res.status(500).send({message: `Err: ${err}`});
      if(!user) return res.status(404).send({message: `No encontrado${user}`});
      res.send('Eliminado la Question');
    })
  };
module.exports = userController;
