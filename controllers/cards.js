var Card = require('../models/card');
const moment = require('moment');
//=========== CONFIG =====================
var config = require('../config/auth');
var apiKey = config.appkey;
var oauthToken = config.accessToken;
var Trello = require('trello-node-api')(apiKey, oauthToken);

var cardController = {};


cardController.createCard = function(req, res, next) {

  //nueva forma de crear allCardsAudit
  var data = {
    name: req.body.name,
    desc: req.body.desc,
    pos: 'top',
    idList: req.body.idList, //REQUIRED
    idLabels: req.body.idLabels,
    due: req.body.due,
    dueComplete: false,
    idMembers: req.body.idMembers,
    urlSource: req.body.urlSource
  };
  Trello.card.create(data).then(function(response) {
    console.log('response ', response);
    console.log('crea card en BD');

    var card = new Card({
      //necesario para crear card
      name: req.body.name,
      desc: req.body.desc,
      due: req.body.due,
      idList: req.body.idList, //REQUIRED
      idLabels: req.body.idLabels,
      idMembers: req.body.idMembers,
      urlSource: req.body.urlSource,
      // card Control
      linkBoard: req.body.linkBoard,
      userAutor: req.body.userAutor,
      audit: req.body.audit
    });

    card.save(function(err) {
      if (err) {
        return next(err);
      }
    });
    res.send('la tarjeta se añadio de forma exitosa')

  })
  .catch(function(error) {
    console.log('error', error);
  });


  // fin ============

/*
  var card = new Card({
    //necesario para crear card
    name: req.body.name,
    desc: req.body.desc,
    idList: req.body.idList, //REQUIRED
    idMembers: req.body.idMembers,
    urlSource: req.body.urlSource,
    // card Control
    linkBoard: req.body.linkBoard,
    userAutor: req.body.userAutor,
    audit: req.body.audit
  });

  card.save(function(err) {
    if (err) {
      return next(err);
    }
    var data = {
      name: req.body.name,
      desc: req.body.desc,
      pos: 'top',
      idList: req.body.idList, //REQUIRED
      due: null,
      dueComplete: false,
      idMembers: req.body.idMembers,
      urlSource: req.body.urlSource
    };
    Trello.card.create(data).then(function(response) {
      console.log('response ', response);
    }).catch(function(error) {
      console.log('error', error);
    });

*/





};
cardController.detailsCard = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  Card.findById(objId)
    .populate({
      path: 'audit',
      select: 'audit_ref'
    })
    .exec(function(err, card) {
      if (err) return res.status(500).send({
        message: `Ocurrio un problema`
      });
      if (!card) return res.status(404).send({
        message: `Not found ${card}`
      });

      res.send(card);

    });
};

cardController.allCard = (req, res) => {

    Card.find({}, (err, cards) => {
      if (err) return res.status(500).send({
        message: `Ocurrio un problema`
      });
      if (!cards) return res.status(404).send({
        message: `Not found`
      });

      res.send(cards);
    })
};

cardController.cardsPerUser = (req, res) => {
  var u =  req.params.user;
  Card.find({userAutor: u}, (err, cards) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!cards) return res.status(404).send({
      message: `Not found`
    });

    res.send(cards);
  })
};

cardController.allCardsAudit = (req, res) => {
  var audit = req.params.audit;
  Card.find()
    .populate({
      path: 'audit',
      select: 'audit_ref'
    })
    .exec(function(err, cards) {
      if (err) return res.status(500).send({
        message: `Ocurrio un problema`
      });
      if (!cards) return res.status(404).send({
        message: `Not found`
      });

      var tarjetas = cards.filter(card => card.audit._id === audit);
      res.send(tarjetas);


    });
};

cardController.updateCard = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;
  console.log(objId);
  console.log(updated);

  Card.findOneAndUpdate({
    _id: objId
  }, updated, (err, card) => {
    if (err) return res.status(500).send({
      message: `No se actualizo  ${card}`
    });
    res.send(card);
  });
};

cardController.deleteCard = (req, res) => {
  let objId = req.params.id;

  Card.findOneAndDelete({
    _id: objId
  }, (err, card) => {
    if (err) return res.status(500).send({
      message: `No se elimino ${card}`
    });
    if (!card) return res.status(404).send({
      message: `No encontrado${card}`
    });
    res.send('Eliminado el enlace');
  })
};


module.exports = cardController;
