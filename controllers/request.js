var Request = require('../models/request_type');

//=========== CONFIG =====================

var requestController = {};

requestController.createRequest = function(req, res, next) {
  var request = new Request({
    name: req.body.name,
    depto: req.body.depto
  });
  request.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('Se añadio el request de forma exitosa')
  })
};

requestController.allRequest = (req, res) => {
  var dpto = req.params.dpto;
  Request.find({status: 1, depto : dpto}, (err, requests) => {
    console.log(err);
    console.log(requests);
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!requests) return res.status(404).send({
      message: `Not found`
    });
    res.send(requests);
  })
};

requestController.todasRequest = (req, res) => {
  var dpto = req.params.dpto;
  Request.find({status: 1}, (err, requests) => {
    console.log(err);
    console.log(requests);
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!requests) return res.status(404).send({
      message: `Not found`
    });
    res.send(requests);
  })
};


requestController.updateRequest = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;

  Request.findOneAndUpdate({
    _id: objId
  }, updated, (err, rq) => {
    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    res.send(rq);
  });
};


requestController.desactivarRq = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  Request.findOneAndUpdate({
    _id: objId
  }, obj, (err, rq) => {
    if (err) return res.status(500).send({
      message: `Err:   ${err}`
    });
    res.send('Eliminado la Solicitud');
  });
};

requestController.activateRq = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 1 };
  Request.findOneAndUpdate({
    _id: objId
  }, obj, (err, rq) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send('Activada la Solicitud');
  });
};

module.exports = requestController;
