var TrelloUser = require('../models/trellouser');
var config = require('../config/auth');
var apiKey = config.appkey;
var oauthToken = config.accessToken;
var Trello = require('trello-node-api')(apiKey, oauthToken);

exports.createTrellouser = function(req, res, next) {
  var trellouser = new TrelloUser({
    email : req.body.email,
    userName : req.body.userName,
    id_user  : req.body.id_user,
    dptoUser     : req.body.dptoUser
  });

  trellouser.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('El trellouser se añadio de forma exitosa')
  })
};

exports.detailsTrellouser = function(req, res, next) {
  TrelloUser.findById(req.params.id, function(err, trellouser) {
    if (err) return next(err);
    res.send(trellouser);
  })
};

exports.datosTrello = function(req, res, next) {
  var email = req.params.email;
  Trello.member.search(email)
  .then(function (response) {
       console.log('response ', response);
       var data = {
         id : response.id,
         username: response.username,
         idBoards: response.idBoards
      };
      res.send(data);

   })
   .catch(function (error) {
       console.log('error', error);
       res.status(500).send(error);

   });

};


exports.allTrellouser = (req, res) => {

  TrelloUser.find({}, (err, trellousers) =>{
    if (err) return res.status(500).send({message: `Ocurrio un problema`});
    if(!trellousers) return res.status(404).send({message: `Not found`});

    res.send(trellousers);


  })
};


exports.updateTrelloUser = (req, res) =>{
  let objId = req.params.id;
  let updated = req.body;
  console.log(objId);
  console.log(updated);

  TrelloUser.findOneAndUpdate({_id: objId}, updated, (err, trellouser) => {
    if (err) return res.status(500).send({message: `No se actualizo  ${trellouser}`});
    res.send(trellouser);
  });
};

exports.deleteTrelloUser = (req, res)=> {
  let objId = req.params.id;

  TrelloUser.findOneAndDelete({_id: objId}, (err,  trellouser) =>{
    if (err) return res.status(500).send({message: `No se elimino ${trellouser}`});
    if(!trellouser) return res.status(404).send({message: `No encontrado${trellouser}`});
    res.send('Eliminado la Question');
  })
};


exports.desactivarTrelloUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  TrelloUser.findOneAndUpdate({
    _id: objId
  }, obj, (err, tu) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send(`desactivado el  usuario ${tu}`);
  });
};

exports.activateTrelloUser = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 1 };
  TrelloUser.findOneAndUpdate({
    _id: objId
  }, obj, (err, tu) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send(`Activado el usuario ${tu}`);
  });
};
