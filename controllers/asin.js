var Asin = require('../models/asin');
var async = require('async');

//=========== CONFIG =====================

var asinController = {};


asinController.createAsin = function(req, res, next) {

  var asin = new Asin({

    brand: req.body.brand,
    canal: req.body.canal,
    asinParent: req.body.asinParent,
    audits: req.body.audits,
    isBusinessReport : req.body.isBusinessReport,
    last_audit: req.body.last_audit,
    sku : req.body.sku

  });
asin.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('se añadio el link de forma exitosa')
  })

};
asinController.detailsAsin = (req, res) => {
  let objId = req.params.id;
  Asin.findById(objId, (err, asin) => {
    if (err) return res.status(500).send({
      message: `Ups, ocurrio un problema ${asin}`
    });
    if (!asin) return res.status(404).send({
      message: `Not found ${asin}`
    });

    res.send(asin);

  })
};


asinController.allAsin = (req, res) => {

  Asin.find({status: 1}, (err, asins) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!asins) return res.status(404).send({
      message: `Not found`
    });

    res.send(asins);
  })
};

asinController.AsinbyBrand = (req, res) => {
  var brand = req.params.brand;
  Asin.find({status: 1, brand:brand}, (err, asins) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!asins) return res.status(404).send({
      message: `Not found`
    });

    res.send(asins);
  })
};


asinController.asinAvailable = (req, res) => {
  //los enlaces disponibles para auditoria, segun el dpto q se requiera
  var brand = req.params.brand;
  console.log(brand);
  var tipoaudit = req.params.tipoa;
  Asin.find({status: 1}, (err, asins) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!asins) return res.status(404).send({
      message: `Not found`
    });
    var resultado = asins.filter(asin => asin.brand.trim());
    console.log('resultado');
    console.log(resultado);
    var marcasOK = resultado.filter(marca => marca.brand.trim() == brand.trim());
    console.log('marcas');
    console.log(marcasOK);
    var availables = marcasOK.filter(asin => !asin.audits.includes(tipoaudit));
    res.send(availables);

  })
};

asinController.marcasRegistradas = (req, res) => {

  Asin.find({status: 1}, {brand:1, _id: 0})
  .exec(function(err, brands) {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!brands) return res.status(404).send({
      message: `Not found`
    });
    let set = new Set( brands.map( JSON.stringify ) );
    let brandOnce = Array.from( set ).map( JSON.parse );
    res.send(brandOnce);

  })

};

asinController.updateAsinAudits = (req, res) => {

  let objId = req.params.id;
  let updated = req.body;
  console.log('id: '+objId);
  console.log('upd:');
  console.log(updated);

  Asin.findOneAndUpdate({
    _id: objId
  }, { $addToSet: { audits: updated.audits } }, (err, asin) => {
    console.log('asin: ');
    console.log(asin);
    if (err) return res.status(500).send({
      message: `No se actualizo  ${asin}`
    });
    res.send(asin);
  });
};

asinController.restarTypeAudit = (req, res) => {
  let objId = req.params.id;
  let item = req.params.item;

  Asin.findOneAndUpdate({
    _id: objId
  }, { $pullAll: { audits: [ item ] } }, (err, asin) => {
    console.log(asin);
    if (err) return res.status(500).send({
      message: `No se actualizo  ${asin}`
    });
    res.send(asin);
  });
};

asinController.updateAsin = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;
  console.log(objId);
  console.log(updated);

  Asin.findOneAndUpdate({
    _id: objId
  }, updated, (err, asin) => {
    console.log(asin);
    if (err) return res.status(500).send({
      message: `No se actualizo  ${asin}`
    });
    res.send(asin);
  });
};


asinController.deleteAsin = (req, res) => {
  let objId = req.params.id;

  Asin.findOneAndDelete({
    _id: objId
  }, (err, asin) => {
    if (err) return res.status(500).send({
      message: `No se elimino ${asin}`
    });
    if (!asin) return res.status(404).send({
      message: `No encontrado${asin}`
    });
    res.send('Eliminado el enlace');
  })
};

asinController.upload_file = (req, res) => {
  var asins = JSON.parse(req.body.asins);
  var response = [];
  Asin.find({}).exec(function(err,asin_obj){
    async.each(asins, function(asin, callback) {
      if (asin_obj) {
        var findasin = {find: false, position: null};
        for (var i in asin_obj) {
          if (asin_obj[i].sku == asin.sku) {
            findasin.find = true;
            findasin.position = i;
          }
        }
        if (!findasin.find) {
          var asinan_obj = new Asin();
          console.log('new!');
        }else {
          var asinan_obj = asin_obj[findasin.position];
          console.log('update!');
        }
      }
      asinan_obj.asinParent = asin.asin;
      asinan_obj.sku = asin.sku;
      asinan_obj.brand = asin.brand.trim();
      asinan_obj.country = asin.country;
      asinan_obj.save(function(err){
        if (err) {
          response.push(err);
        }else {
          response.push(asinan_obj.sku);
        }
        callback();
      });
    }, function(err) {
      res.json(response);
    });
  });
};

module.exports = asinController;
