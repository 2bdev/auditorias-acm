var Audit = require('../models/audit');
const Asin = require('../models/asin');
const User = require('../models/user');
const Card = require('../models/card');
var Question = require('../models/question');
const moment = require('moment');
//=========== CONFIG =====================

var auditController = {};


auditController.createAudit = function(req, res, next) {

  //====================nuevo audit =====================
  var reqAudit = req.body;
  Audit.findOne({}).sort({
    createdAt: -1
  }).exec(function(err, LastAudit) {
    var LastAudit_nro = LastAudit ? LastAudit.audit_nro : 0;
    var newAudit = new Audit();
    newAudit.audit_nro = LastAudit_nro + 1;
    newAudit.tipo = reqAudit.tipo;
    newAudit.link = reqAudit.link;
    newAudit.usuario = reqAudit.usuario;
    newAudit.country = reqAudit.country;
    newAudit.audit_ref = `${reqAudit.tipo}_${reqAudit.country}_${LastAudit_nro + 1}`;
    newAudit.evaluacion = reqAudit.evaluacion;

    //===================save======================
    console.log(newAudit);

    newAudit.save(function(err) {
      if (err) {
        return next(err);
      }
      res.send('se añadio el audit de forma exitosa')
    })
  });

};
//=========================




auditController.detailsAudit = (req, res) => {
  let objId = req.params.id;
  Audit.findById(objId, (err, audit) => {
    if (err) return res.status(500).send({
      message: `Ups, ocurrio un problema ${audit}`
    });
    if (!audit) return res.status(404).send({
      message: `Not found ${audit}`
    });

    res.send(audit);

  })
};

auditController.ultimateAudit = (req, res) => {
  let user = req.params.user;
  let dpto = req.params.dpto;

  Audit.find({usuario: user, tipo: dpto}).sort({createdAt: -1})
  .exec(function(err, audits) {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!audits) return res.status(404).send({
      message: `Not found`
    });
    console.log(audits[0]);
    res.send(audits[0]);
  });

};

auditController.allAudit = (req, res) => {

  Audit.find({}, (err, audits) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!audits) return res.status(404).send({
      message: `Not found`
    });

    res.send(audits);
  })
};

auditController.auditsPerUser = (req, res) => {
  var u =  req.params.user;
  Audit.find({usuario: u}, (err, audits) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!audits) return res.status(404).send({
      message: `Not found`
    });

    res.send(audits);
  })
};


auditController.reportAudit = (req, res) => {
  Audit.find({}, (err, audits) => {

    var dataset = [];

    for (var i = 0; i < audits.length; i ++){

      if(audits[i].evaluacion.length != 0){
        console.log(audits[i].evaluacion);
        for (var a = 0; a < audits[i].evaluacion.length; a ++){

          if(audits[i].evaluacion[a].result.length != 0){
            for (var u = 0; u < audits[i].evaluacion[a].result.length; u ++){
              if(audits[i].evaluacion[a].result[u].r == true){
                var fila = {}
                fila.nro =  audits[i].audit_nro ? audits[i].audit_nro : '-'
                fila.fecha = audits[i].fecha_audit ?  moment(audits[i].fecha_audit).format("MM/DD/YYYY hh:mm a") : '-'
                fila.tipo =  audits[i].tipo.name ? audits[i].tipo.name : '-'
                fila.asin =   audits[i].link ? audits[i].link.asinParent : '-'
                fila.sku =   audits[i].link ? audits[i].link.sku : '-'
                fila.ref =   audits[i].audit_ref ? audits[i].audit_ref : '-'

                fila.brand =  audits[i].link ? audits[i].link.brand : '-'
                fila.user =  audits[i].usuario ? audits[i].usuario.email : '-'

                fila.result =  audits[i].result ? audits[i].result : '-'

                fila.variable = audits[i].evaluacion[a] ? audits[i].evaluacion[a].variable : '-'
                fila.porcent =  audits[i].evaluacion[a] ? audits[i].evaluacion[a].porcentaje : '-'

                fila.p = audits[i].evaluacion[a] ? audits[i].evaluacion[a].result[u].p : '-'
                fila.r = audits[i].evaluacion[a] ?  audits[i].evaluacion[a].result[u].r : '-'


                dataset.push(fila);
              }

            }
          }
        }
      }


      }



//===============================================
    if (err) return res.status(500).send({
      message: `Err: ${err}`
    });
    if (!audits) return res.status(404).send({
      message: `Not found`
    });
    res.send(dataset);
  })
};



//=========================




auditController.reportPorUsuario = (req, res) => {
  var user = req.params.iduser;
  Audit.find({usuario: user}, (err, audits) => {

    var dataset = [];

    for (var i = 0; i < audits.length; i ++){

      if(audits[i].evaluacion.length != 0){
        console.log(audits[i].evaluacion);
        for (var a = 0; a < audits[i].evaluacion.length; a ++){

          if(audits[i].evaluacion[a].result.length != 0){
            for (var u = 0; u < audits[i].evaluacion[a].result.length; u ++){
              if(audits[i].evaluacion[a].result[u].r == true){
                var fila = {}
                fila.nro =  audits[i].audit_nro ? audits[i].audit_nro : '-'
                fila.fecha = audits[i].fecha_audit ?  moment(audits[i].fecha_audit).format("MM/DD/YYYY hh:mm a") : '-'
                fila.tipo =  audits[i].tipo.name ? audits[i].tipo.name : '-'
                fila.asin =   audits[i].link ? audits[i].link.asinParent : '-'
                fila.sku =   audits[i].link ? audits[i].link.sku : '-'
                fila.ref =   audits[i].audit_ref ? audits[i].audit_ref : '-'

                fila.brand =  audits[i].link ? audits[i].link.brand : '-'
                fila.user =  audits[i].usuario ? audits[i].usuario.email : '-'

                fila.result =  audits[i].result ? audits[i].result : '-'

                fila.variable = audits[i].evaluacion[a] ? audits[i].evaluacion[a].variable : '-'
                fila.porcent =  audits[i].evaluacion[a] ? audits[i].evaluacion[a].porcentaje : '-'

                fila.p = audits[i].evaluacion[a] ? audits[i].evaluacion[a].result[u].p : '-'
                fila.r = audits[i].evaluacion[a] ?  audits[i].evaluacion[a].result[u].r : '-'


                dataset.push(fila);
              }

            }
          }
        }
      }


      }



//===============================================
    if (err) return res.status(500).send({
      message: `Err: ${err}`
    });
    if (!audits) return res.status(404).send({
      message: `Not found`
    });
    res.send(dataset);
  })
};



//=========================


auditController.updateAudit = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;
  console.log(objId);
  console.log(updated);

  Audit.findOneAndUpdate({
    _id: objId
  }, updated, (err, audit) => {
    if (err) return res.status(500).send({
      message: `No se actualizo  ${audit}`
    });
    res.send(audit);
  });
};



auditController.deleteAudit = (req, res) => {
  let objId = req.params.id;

  Audit.findOneAndDelete({
    _id: objId
  }, (err, audit) => {
    if (err) return res.status(500).send({
      message: `No se elimino ${audit}`
    });
    if (!audit) return res.status(404).send({
      message: `No encontrado${audit}`
    });
    res.send('Eliminado el enlace');
  })
};

module.exports = auditController;
