var Departamentos = require('../models/department');
var Request = require('../models/request_type');
var config = require('../config/auth');
var apiKey = config.appkey;
var oauthToken = config.accessToken;
var Trello = require('trello');
var trello = new Trello(apiKey, oauthToken);
//=========== CONFIG =====================

var departamentosController = {};

departamentosController.allDepartamentos = (req, res) => {
  Departamentos.find({status: 1}).exec(function(err, deptos) {
    console.log(deptos);
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!deptos) return res.status(404).send({
      message: `Not found`
    });


    res.send(deptos);
  });
};


departamentosController.detailsDpto = (req, res) => {
  let objId = req.params.id;
  Departamentos.findById(objId, (err, dpto) => {
    if (err) return res.status(500).send({
      message: `Err ${err}`
    });
    if (!dpto) return res.status(404).send({
      message: `Not found ${dpto}`
    });

    res.send(dpto);

  })
};



departamentosController.dptosVar = (req, res) => {
    var id = req.params.id;
    Departamentos.findById(id, (err, dpto) => {
      if (err) return res.status(500).send({
        message: `Ups, ocurrio un problema ${err}`
      });
      if (!dpto) return res.status(404).send({
        message: `Not found ${dpto}`
      });

      res.send(dpto.variables);

    })
};


departamentosController.createDepartamentos = function(req, res) {
  var departamentos = new Departamentos({
    name: req.body.name,
    idBoard: req.body.idBoard,
    linkBoard: req.body.linkBoard,
    variables: req.body.variables,
    idList: req.body.idList,
    tipo: req.body.tipo,
    nameBoard: req.body.nameBoard,
    nameList: req.body.nameList
  });
  departamentos.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('El Departamento se añadió de forma exitosa')
  })
};

departamentosController.datosListTrello = function(req, res){
          var idboard = req.params.idboard;
          var datos = {}
          trello.getListsOnBoard(idboard)
          .then((lista) => {
              datos.idBoard = lista[0].idBoard;
              datos.idList = lista[0].id;
              datos.listName = lista[0].name;
              res.send(datos);
            })
            .catch(err => {
                res.send(`Err: ${err}`);
            })

}


departamentosController.datosBoardTrello = function(req, res){
          var member = req.params.member;
          trello.getBoards(member)
          .then((boards) => {
              res.send(boards);
            })
            .catch(err => {
                res.send(`Err: ${err}`);
            })
}

departamentosController.datosLabelsTrello = function(req, res){
          var board = req.params.board;
          trello.getLabelsForBoard(board)
          .then((boards) => {
            console.log(boards);
              res.send(boards);
            })
            .catch(err => {
                res.send(`Err: ${err}`);
            })
}
departamentosController.updateDpto = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;

  Departamentos.findOneAndUpdate({
    _id: objId
  }, updated, (err, dpto) => {
    if (err) return res.status(500).send({
      message: `No se actualizo  ${dpto}`
    });
    res.send(dpto);
  });
};

departamentosController.desactivarDpto = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  Departamentos.findOneAndUpdate({
    _id: objId
  }, obj, (err, dpto) => {
    if (err) return res.status(500).send({
      message: `Err: ${err}`
    });
    res.send(`Eliminado el Usuario ${dpto}`);
  });
};

departamentosController.activateDpto = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 1 };
  Departamentos.findOneAndUpdate({
    _id: objId
  }, obj, (err, dpto) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send('Activado el dpto');
  });
};


module.exports = departamentosController;
