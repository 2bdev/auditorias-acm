var Country = require('../models/country');

//=========== CONFIG =====================

var countryController = {};

countryController.createCountry = function(req, res, next) {
  var country = new Country({
    name: req.body.name,
    url: req.body.link
  });
  country.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('Se añadio el country de forma exitosa')
  })
};

countryController.allCountry = (req, res) => {
  Country.find({}, (err, countries) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!countries) return res.status(404).send({
      message: `Not found`
    });
    res.send(countries);
  })
};

module.exports = countryController;
