var Question = require('../models/question');

var questionController = {};

questionController.createQuestion = function(req, res) {
  var question = new Question({
    contentQuestion: req.body.contentQuestion,
    typeQuestion :   req.body.typeQuestion,  //(req.body.typeQuestion == 1)? 'titulo':'descycont',
    dptoQuestion: req.body.dptoQuestion,
    linkBoard: req.body.linkBoard,
    idList: req.body.idList
  });

  question.save(function(err) {
    if (err) {
      return next(err);
    }
    res.send('la pregunta se añadio de forma exitosa')
  })
};

questionController.detailsQuestion = (req, res) => {
  let objId = req.params.id;
  Question.findById(objId, (err, question) => {
    if (err) return res.status(500).send({
      message: `Ups, ocurrio un problema ${question}`
    });
    if (!question) return res.status(404).send({
      message: `Not found ${question}`
    });

    res.send(question);

  })
};

questionController.allQuestion = (req, res) => {

  Question.find({}, (err, questions) => {
    if (err) return res.status(500).send({
      message: `Error: ${err}`

    });
    if (!questions) return res.status(404).send({
      message: `Not found`
    });

    res.send(questions);


  })
};


questionController.questionDpto = (req, res) => {

  Question.find({statusQuestion: 1}, (err, questions) => {

    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    if (!questions) return res.status(404).send({
      message: `Not found`
    });
    var qto = [];
    for(var i=0; i<questions.length; i++ ){
        console.log(questions[i].dptoQuestion);
          qto.push(questions[i].dptoQuestion);

    }
    Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
    });

    var result = qto.unique();
    res.send(result);


  })
};


questionController.questionVariables = (req, res) => {

    var dpto = req.params.dpto;
  Question.find({statusQuestion: 1}, (err, questions) => {

    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    if (!questions) return res.status(404).send({
      message: `Not found`
    });
    var variables = questions.filter(qto => qto.dptoQuestion == dpto );
    res.send(variables);
    });

};



questionController.questionTipoUno = (req, res) => {

  Question.find({}, (err, questions) => {

    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    if (!questions) return res.status(404).send({
      message: `Not found`
    });
    var qtipouno = [];
    for(var i=0; i<questions.length; i++ ){
        console.log(questions[i].typeQuestion);
          if(questions[i].typeQuestion == 1){
              qtipouno.push(questions[i]);
          }

    }

    res.send(qtipouno);


  })
};


questionController.questionTipoDos = (req, res) => {

  Question.find({}, (err, questions) => {

    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    if (!questions) return res.status(404).send({
      message: `Not found`
    });
    var qtipodos = [];
    for(var i=0; i<questions.length; i++ ){
        console.log(questions[i].typeQuestion);
          if(questions[i].typeQuestion == 2){
              qtipouno.push(questions[i]);
          }

    }

    res.send(qtipodos);


  })
};


questionController.updateQuestion = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;
  if (updated.typeQuestion) {
    updated.typeQuestion = (updated.typeQuestion == 1)? 'titulo':'descycont'
  }
  Question.findOneAndUpdate({
    _id: objId
  }, updated, (err, question) => {
    if (err) return res.status(500).send({
      message: `Error: ${err}`
    });
    res.send(question);
  });
};

questionController.deleteQuestion = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { statusQuestion: 0 };
  Question.findOneAndUpdate({
    _id: objId
  }, obj, (err, question) => {
    if (err) return res.status(500).send({
      message: `No se elimino  ${question}`
    });
    res.send('Eliminado la Question');
  });
};

questionController.activateQuestion = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { statusQuestion: 1 };
  Question.findOneAndUpdate({
    _id: objId
  }, obj, (err, question) => {
    if (err) return res.status(500).send({
      message: `No se activo  ${question}`
    });
    res.send('Activada la Question');
  });
};

questionController.questionAudit = (req, res) => {
  var dpto = req.params.depto;
  Question.find({dptoQuestion:dpto, statusQuestion: 1}, (err, questions) =>{
    if ( !err && questions.length > 0 ) {
      var qActivasDpto = questions.filter(qto => qto.dptoQuestion._id == dpto && qto.statusQuestion != 0);
      var tiposQuestions = [];
      if(qActivasDpto.length > 0){
        for(var i=0; i<qActivasDpto.length; i++ ){
          if(qActivasDpto[i].typeQuestion != '' && qActivasDpto[i].typeQuestion != undefined ){
              tiposQuestions.push(qActivasDpto[i].typeQuestion);
          }

        }
      }
      Array.prototype.unique=function(a){
        return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
      });
      var tipos = tiposQuestions.unique();
      res.json({tipos:tipos, questions:questions})
    }
  });
};

module.exports = questionController;
