var Brand = require('../models/brand');

var brandController = {};


brandController.createBrand = function(req, res, next) {
  var brand = new Brand({
    name: req.body.name,
    shortName: req.body.shortName,
    
  });
  brand.save(function(err) {
    if (err)
      throw err;
    res.send(`La marca se añadio exitosamente ${brand}`)
  });



};


brandController.allBrand = (req, res) => {
  Brand.find({}, (err, brands) => {
    if (err) return res.status(500).send({
      message: `Ocurrio un problema`
    });
    if (!brands) return res.status(404).send({
      message: `Not found`
    });
    res.send(brands);
  })
};

brandController.updateBrand = (req, res) => {
  let objId = req.params.id;
  let updated = req.body;

  Brand.findOneAndUpdate({_id: objId}, updated, (err, brand) => {
    if (err) return res.status(500).send({message: `No se actualizo  ${brand}`});
    res.send(brand);
  });
};


brandController.desactivarBrand = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 0 };
  Brand.findOneAndUpdate({
    _id: objId
  }, obj, (err, brand) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send(`desactivado la marca ${brand}`);
  });
};

brandController.activateBrand = (req, res) => {
  let objId = req.params.id;
  console.log(objId);
  var obj = { status: 1 };
  Brand.findOneAndUpdate({
    _id: objId
  }, obj, (err, brand) => {
    if (err) return res.status(500).send({
      message: `Err:  ${err}`
    });
    res.send(`Activado la marca ${brand}`);
  });
};



module.exports = brandController;
