module.exports = function(app, passport) {
  var Question = require('../models/question');
  var User = require('../models/user');
  var mid = require('../routes/middleware');
  var gf = require('../routes/general_functions');
  //===========Vista de inicio==============
  app.get('/', function(req, res) {
    res.redirect('/users/login');
  });

  // ===============FIRST LOGIN =================
  // process the login form
  app.post('/login', passport.authenticate('local-login', {
    successRedirect: '/users/profile', // redirect to the secure profile section
    failureRedirect: '/users/login', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }));

  // =====================================
  // SIGNUP ==============================
  // =====================================


  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/users', // redirect to the secure profile section
    failureRedirect: '/users/signup', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }));



  // =============================================================================
  // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
  // =============================================================================



  // TRELLO --------------------------------


  // send to trello to do the authentication
  //app.get('/connect/trello',mid.isLoggedIn ,passport.authorize('trello'));

  // handle the callback after trello has authorized the user


  /*    app.get('/connect/trello/callback', passport.authorize('trello',{
        failureRedirect: '/error'
      }),function(req, res) {
             console.log(req.session);
            res.redirect('/users/profile');
          });*/
  // =============================================================================
  // UNLINK ACCOUNTS =============================================================
  // =============================================================================
  // used to unlink accounts. for social accounts, just remove the token
  // for local account, remove email and password
  // user account will stay active in case they want to reconnect in the future
  app.get('/unlink/local', function(req, res) {
    var user = req.user;
    user.local.email = undefined;
    user.local.password = undefined;
    user.save(function(err) {
      res.redirect('/users/profile');
    });
  });


  // --------------------------------
  /*	app.get('/unlink/trello', function(req, res) {
  		var user           = req.user;
  		user.trello.tokenKey = undefined;
      user.save(function(err) {
  			res.redirect('/users/profile');
  		});
  	});*/




  app.get('/report', mid.isLoggedIn, function(req, res) {
    // render the page and pass in any flash data if it exists
    res.render('pages/report', {
      title: 'Listado de Auditorias',
      extractScripts: true,
      extractStyles: true,
      user: req.user
    });
  });

  //=====================RUTAS DE ADMIN================================




  //=====================================================


};
