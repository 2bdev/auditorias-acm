var express = require('express');
var router = express.Router();
var user = require("../controllers/user");
var User = require('../models/user');
var Asin = require('../models/asin');
var Brand = require('../models/brand');
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');

//============show the login form========
router.get('/login', function(req, res) {
  // render the page and pass in any flash data if it exists
  res.render('pages/login', {
    message: req.flash('loginMessage'),
    title: 'Iniciar Sesion',
    layout: 'layouts/users',
    extractScripts: true,
    extractStyles: true
  });
});

// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});
// show the signup form
 router.get('/signup', function(req, res) {
   // render the page and pass in any flash data if it exists
   res.render('pages/signup', {
     message: req.flash('signupMessage'),
     title: 'Registrar Usuario',
     layout: 'layouts/users',
     extractScripts: true,
     extractStyles: true,
  });
 });
// =====================================
// PROFILE SECTION =====================
// =====================================
// we will want this protected so you have to be logged in to visit
// we will use route middleware to verify this (the isLoggedIn function)
router.get('/profile', mid.isLoggedIn, function(req, res) {
  
  res.render('pages/profile', {
      title: 'Perfil del Usuario',
      layout: 'layouts/main',
      breadcrumb: gf.breadcrumb(true, "Usuarios", "", "Perfil"),
      extractScripts: true,
      extractStyles: true,
      user : req.user,
      trellouser: req.user.trellouser // get the user out of session and pass to template
  });
});
router.get('/', mid.isLoggedIn, mid.isAdmin, function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin' ){
    Brand.find({})
    .exec(function(err,brands){
      //let set = new Set( brands.map( JSON.stringify ) )
      //let brandOnce = Array.from( set ).map( JSON.parse );

      res.render('pages/register/users', {
        title: 'Registro de Usuarios',
        layout: 'layouts/main',
        message: req.flash('signupMessage'),
        breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Usuarios"),
        extractScripts: true,
        extractStyles: true,
        user : req.user,
        data : brands
      });
    })
  }else{
    res.redirect('/404');
  }
});

//crear usuario
router.post('/create/user', user.createUser);
//obtener un usuario por id
router.get('/get/user/:id', user.detailsUser);
//obtener una lista de usuarios
router.get('/get/users', user.allUser);
//Editar un usuario
router.put('/update/user/:id', user.updateUser);
//eliminar un usuario
router.delete('/delete/user/:id', user.deleteUser);
//eliminar un usuario
router.delete('/eliminar/user/:id', user.eliminarUsuario);

//desactivar un usuario
router.delete('/desactivar/user/:id', user.statusUser);
//desactivar un usuario
router.delete('/activate/user/:id', user.activateUser);
//confirmar pass
router.get('/confirmar/user/:id/:pass', user.confirmPass);
//nueva pass
router.put('/cambiarpass/user/:id', user.updatePass);


module.exports = router;
