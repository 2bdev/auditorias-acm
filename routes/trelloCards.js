var express = require('express');
var router = express.Router();
var trellocard = require("../controllers/cards");
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');
var request = require("request");

//============== crear tarjetas ===================
router.post('/create/card', trellocard.createCard);
router.get('/get/cards', trellocard.allCard);
router.get('/get/miscards/:user', trellocard.cardsPerUser);
router.put('/update/card/:id', trellocard.updateCard);
router.get('/get/card/:id', trellocard.detailsCard);
router.get('/get/card/:audit', trellocard.allCardsAudit);
router.get('/delete/card/:id', trellocard.deleteCard);




module.exports = router;
