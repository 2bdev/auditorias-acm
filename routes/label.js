var express = require('express');
var router = express.Router();
var label = require("../controllers/label");
var gf = require('../routes/general_functions');
var mid = require('../routes/middleware');
var request = require("request");
var Label = require('../models/label');

/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin'){
    res.render('pages/register/label', {
      title: 'Registro de Etiquetas',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Labels"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//crear un label
router.post('/create/label', label.createLabel);
//get label
router.get('/get/labels', label.allLabel);

module.exports = router;
