var express = require('express');
var router = express.Router();
var request = require("../controllers/request");
var gf = require('../routes/general_functions');
var mid = require('../routes/middleware');
var Request = require('../models/request_type');

/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin'){
    res.render('pages/register/request', {
      title: 'Registro de Tipo de Solicitud',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Tipos de Solicitud"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//crear un request
router.post('/create/request', request.createRequest);
//get request
router.get('/get/requests/:dpto', request.allRequest);
//get request
router.get('/get/requests', request.todasRequest);
//update request
router.put('/update/request/:id', request.updateRequest);

//desactivar request
router.delete('/desactivar/request/:id', request.desactivarRq);
//activar request
router.delete('/activate/request/:id', request.activateRq);

module.exports = router;
