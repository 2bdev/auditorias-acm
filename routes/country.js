var express = require('express');
var router = express.Router();
var country = require("../controllers/country");
var gf = require('../routes/general_functions');
var mid = require('../routes/middleware');
var request = require("request");
var Country = require('../models/country');

/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin'){
    res.render('pages/register/country', {
      title: 'Registro de Paises',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Pais"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//crear un country
router.post('/create/country', country.createCountry);
//get country
router.get('/get/countries', country.allCountry);

module.exports = router;
