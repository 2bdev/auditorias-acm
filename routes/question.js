var express = require('express');
var router = express.Router();
var question = require("../controllers/question");
var Question = require('../models/question');
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');

router.get('/', mid.isLoggedIn, mid.isAdmin, function(req, res, next) {
  Question.find({}, (err, questions) =>{
    if ( !err ) {
      // render the page and pass in any flash data if it exists
      res.render('pages/register/questions', {
        layout: 'layouts/main',
        title: 'Registro de preguntas',
        message: req.flash('registerQ'),
        breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Registro de preguntas"),
        extractScripts: true,
        extractStyles: true,
        user: req.user,
        questions: questions
      });
    }
  });
});
// crear una pregunta
router.post('/create/question', question.createQuestion);
//obtener una pregunta por id
router.get('/get/question/:id', question.detailsQuestion);
//obtener una lista de preguntas
router.get('/get/questions', question.allQuestion);
//obtener una lista de departamentos
router.get('/get/dptos', question.questionDpto);
//obtener una lista de departamentos
router.get('/get/questions/:dpto', question.questionVariables);
//obtener una lista de preguntas
router.get('/get/questions/tipouno', question.questionTipoUno);
//obtener una lista de preguntas
router.get('/get/questions/tipodos', question.questionTipoDos);
//Editar una pregunta
router.put('/update/question/:id', question.updateQuestion);
//eliminar una pregunta
router.delete('/delete/question/:id', question.deleteQuestion);
//activar una pregunta
router.delete('/activate/question/:id', question.activateQuestion);
//obtener preguntas para audits
router.get('/get/question_audit/:depto', question.questionAudit);

module.exports = router;
