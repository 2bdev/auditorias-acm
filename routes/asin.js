var express = require('express');
var router = express.Router();
var asin = require("../controllers/asin");
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');
var request = require("request");
/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin' ){
    res.render('pages/register/asins', {
      title: 'Asins',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Asin"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//crear un link
router.post('/create/asin', asin.createAsin);
//obtener un enlace
router.get('/get/asin/:id', asin.detailsAsin);
//obtener los enlaces
router.get('/upload_file', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin'){
    res.render('pages/register/upload_file_asin', {
      title: 'Asins',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Upload Asin"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//upload file
router.post('/upload_file', asin.upload_file);
//obtener los enlaces
router.get('/get/asins', asin.allAsin);
//obtener enlaces disponibles para auditoria, segun el tipo (dpto)
router.get('/get/asins_brand/:brand', asin.AsinbyBrand);
//obtener enlaces disponibles para auditoria, segun el tipo de auditorias
//es decir retorna los asin a los que no se le ha realizado la auditoria :tipoa
router.get('/get/audit/:tipoa/brand/:brand', asin.asinAvailable);
//obtener brands registradas
router.get('/get/brands', asin.marcasRegistradas);
//modificar enlace
router.put('/update/asin/:id', asin.updateAsin);
//modificar auditorias del asin
router.put('/update/asin/audits/:id', asin.updateAsinAudits);
//eliminar tipo de audit realizado al enlace
router.put('/update/asin/audits/:id/:item', asin.restarTypeAudit);
//eliminar enlace
router.delete('/delete/asin/:id', asin.deleteAsin);



module.exports = router;
