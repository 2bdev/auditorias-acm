var express = require('express');
var router = express.Router();
var audit = require("../controllers/audit");
var Question = require('../models/question');
var Asin = require('../models/asin');
var Country = require('../models/country');
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');

router.get('/products/:marketplace/:dpto', mid.isLoggedIn, function(req, res) {
  var marketplace = req.query.marketplace;
  var dpto = req.params.dpto;
  console.log('dpto');
  console.log(dpto);
  Question.find({}, (err, questions) =>{
    if ( !err && questions.length > 0 ) {
      var qActivasDpto = questions.filter(qto => qto.dptoQuestion == dpto && qto.statusQuestion != 0);
      var tiposQuestions = [];
      if(qActivasDpto.length > 0){
        for(var i=0; i<qActivasDpto.length; i++ ){
          tiposQuestions.push(qActivasDpto[i].typeQuestion);
        }
      }
      Array.prototype.unique=function(a){
        return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
      });
      console.log(tiposQuestions);
      var tipos = tiposQuestions.unique();
      tipos.push('fin');
      console.log(tipos);
      // render the page and pass in any flash data if it exists
      res.render('pages/auditorias/auditoria', {
        layout: 'layouts/main-audit',
        title: 'Auditar Productos de Amazon',
        extractScripts: true,
        extractStyles: true,
        user: req.user,
        questions: qActivasDpto,
        tipos : tipos,
        dpto: dpto,
        market: marketplace
      });
    }
  });
});

router.get('/products/:marketplace', mid.isLoggedIn, function(req, res) {
  var marketplace = req.query.marketplace;
  res.render('pages/auditorias/stepaudit', {
    layout: 'layouts/main',
    title: 'Auditar Productos de Amazon',
    breadcrumb: gf.breadcrumb(true, "Auditorias", "", "Amazon"),
    extractScripts: true,
    extractStyles: true,
    user: req.user,
    trello: req.user.trellouser._id
  });
});



//crear un audit
router.post('/create/audit', audit.createAudit);
//obtener audit
router.get('/get/audit/:id', audit.detailsAudit);
//obtener audits
router.get('/get/audits', audit.allAudit);
router.get('/get/misaudits/:user', audit.auditsPerUser);

//Report audits
router.get('/report/audits', audit.reportAudit);

//Report audits
router.get('/report/user/:iduser', audit.reportPorUsuario);
//Obtener ultimo audit de un usuario
router.get('/get/ultimateaudit/:user/:dpto', audit.ultimateAudit);
//modificar audit
router.put('/update/audit/:id', audit.updateAudit);
//eliminar audit
router.delete('/delete/audit/:id', audit.deleteAudit);

module.exports = router;
