//=====================middleware============================
// route middleware to make sure a user is logged in

module.exports = {
  isLoggedIn: function(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
      return next();
    }
    // if they aren't redirect them to the home page
    res.redirect('/');
  },
  isAuthorize: function(req, res, next) {
    if (req.user.trellouser){
      return next();
        }
      res.redirect('/404');
  },
  isAdmin: function(req, res, next) {
    
    if (req.user.tipo == 'admin'){
      return next();
        }else if (req.user.tipo == 'superadmin'){
      return next();
      }
      res.redirect('/logout');
  }

};
