var express = require('express');
var router = express.Router();
var trellouser = require("../controllers/trellousers");
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');
var Departamentos = require('../models/department');


//vista trello users

router.get('/', mid.isLoggedIn, mid.isAdmin, function(req, res) {
  console.log(req.user);
  if(req.user.tipo == 'superadmin'){

    Departamentos.find({})
    .exec(function(err, deptos) {

      res.render('pages/register/trellouser', {
        title: 'Registro de Usuarios de trello',
        layout: 'layouts/main',
        message: req.flash('signupMessage'),
        breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Usuarios de Trello"),
        extractScripts: true,
        extractStyles: true,
        user : req.user,
        data: deptos
      });

});

  }else{
    res.redirect('/404');
  }
});





// crear usuario de trello
router.post('/create/trellouser', trellouser.createTrellouser);
//obtener una pregunta por id
router.get('/get/trellouser/:id', trellouser.detailsTrellouser);
//obtener datos de un usuario en trello
router.get('/get/datostrello/:email', trellouser.datosTrello);
//obtener una lista de preguntas
router.get('/get/trellousers', trellouser.allTrellouser);
//Editar una pregunta
router.put('/update/trellouser/:id', trellouser.updateTrelloUser);
//eliminar una pregunta
router.delete('/delete/trellouser/:id', trellouser.deleteTrelloUser);
//desactivar un usuario
router.delete('/desactivar/trellouser/:id', trellouser.desactivarTrelloUser);
//activar un usuario
router.delete('/activate/trellouser/:id', trellouser.activateTrelloUser);

module.exports = router;
