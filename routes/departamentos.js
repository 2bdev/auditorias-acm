var express = require('express');
var router = express.Router();
var departamentos = require("../controllers/departamentos");
var gf = require('../routes/general_functions');
var mid = require('../routes/middleware');
var request = require("request");
var Departamentos = require('../models/department');

/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin' ){
  res.render('pages/register/departamentos', {
    title: 'Registro de Departamentos',
    layout: 'layouts/main',
    message: req.flash('signupMessage'),
    breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Departamentos"),
    extractScripts: true,
    extractStyles: true,
    user : req.user
  });
}else{

    res.redirect('/404');
}


});

//crear un departamentos
router.post('/create/departamentos', departamentos.createDepartamentos);
// //obtener un departamentos
router.get('/get/departamento/:id', departamentos.detailsDpto);
//obtener los departamentos
router.get('/get/departamentos', departamentos.allDepartamentos);
//obtener los departamentos
router.get('/get/variables/dpto/:id', departamentos.dptosVar);
//obtener las idList
router.get('/get/listtrello/:idboard', departamentos.datosListTrello);
//obtener los tableros
router.get('/get/boardtrello/:member', departamentos.datosBoardTrello);
//obtener los labels
router.get('/get/labeltrello/:board', departamentos.datosLabelsTrello);
//editar Dpto
router.get('/update/departamento/:id', departamentos.updateDpto);
// //modificar departamentos
// router.put('/update/departamentos/:id', departamentos.updateDepartamentos);
//desactivar departamentos
router.delete('/desactivar/departamento/:id', departamentos.desactivarDpto);
router.delete('/activate/departamento/:id', departamentos.activateDpto);



module.exports = router;
