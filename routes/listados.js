var express = require('express');
var router = express.Router();
var audit = require("../controllers/audit");
var Cards = require('../models/card');
var Audit = require('../models/audit');
var mid = require('../routes/middleware');
var gf = require('../routes/general_functions');

router.get('/auditorias', mid.isLoggedIn, function(req, res) {
  res.render('pages/listados/audits_tipos', {
    layout: 'layouts/main',
    title: 'Listado de Auditorias Realizadas',
    breadcrumb: gf.breadcrumb(true, "Listados", "", "Auditorias"),
    extractScripts: true,
    extractStyles: true,
    user: req.user
  });
});

router.get('/tarjetas', mid.isLoggedIn, function(req, res, next) {
  res.render('pages/listados/cards', {
    layout: 'layouts/main',
    title: 'Listado de tarjetas creadas',
    breadcrumb: gf.breadcrumb(true, "Listados", "", "Tarjetas de trello"),
    extractScripts: true,
    extractStyles: true,
    user: req.user
  });
});





module.exports = router;
