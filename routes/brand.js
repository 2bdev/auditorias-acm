var express = require('express');
var router = express.Router();
var brand = require("../controllers/brand");
var gf = require('../routes/general_functions');
var mid = require('../routes/middleware');
var Brand = require('../models/brand');

/*Show all*/
router.get('/', mid.isLoggedIn, mid.isAdmin ,function(req, res) {
  if(req.user.tipo == 'superadmin' || req.user.tipo == 'admin' ){
    res.render('pages/register/brand', {
      title: 'Registro de Marcas',
      layout: 'layouts/main',
      message: req.flash('signupMessage'),
      breadcrumb: gf.breadcrumb(true, "Registrar Data", "", "Marcas"),
      extractScripts: true,
      extractStyles: true,
      user : req.user
    });
  }else{
    res.redirect('/404');
  }
});
//crear un country
router.post('/create/brand', brand.createBrand);
//get country
router.get('/get/brands', brand.allBrand);
//desactivar
router.delete('/desactivar/brand/:id', brand.desactivarBrand);
//activar una pregunta
router.delete('/activate/brand/:id', brand.activateBrand);

module.exports = router;
