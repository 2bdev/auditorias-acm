module.exports = {
    breadcrumb: function( show = false, title_parent = "", url_parent = "", title_page = ""){
      var breadcrumb ={};
      breadcrumb.show = show;
      breadcrumb.title_padre = title_parent;
      breadcrumb.url_padre = url_parent;
      breadcrumb.title_page = title_page;
      return breadcrumb;
    }
};
