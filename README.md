# Audit Project 2be

Este proyecto permite realizar auditorias de un listing cargado en un marketplace (Amazon) y generar solicitudes de modificacion de listing a través de una tarjeta en trello.

## Para Colaborar
```bash
git clone (url-repo)
```
En el directorio del proyecto
```bash
npm install
```


## Pre-requisitos
Esta aplicación requiere
- Node.js
- Express
- Mongodb

Se necesitan credenciales para el uso de trello API,  y una bd disponible.


